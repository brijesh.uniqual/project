
import 'package:flutter/material.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List<Offset?> points = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Paint"),
      ),
      body: Center(
        child: Column(
          children: [
            Row(
              children: const [
                Text("data"),
              ],
            ),
            Expanded(
              child: GestureDetector(
                onPanUpdate: (DragUpdateDetails detail) {
                  setState(() {
                    RenderBox? renderBoxObject = context.findRenderObject() as RenderBox?;

                    Offset localDataPoint = renderBoxObject!.globalToLocal(detail.globalPosition);

                    points.add(localDataPoint);

                  });
                },
                onPanEnd: (DragEndDetails details) {
                  points.add(null);
                  setState(() {});
                },
                child: CustomPaint(
                  painter: Painter(points: points),
                  size: Size.infinite,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Painter extends CustomPainter {

  List<Offset?> points;

  Painter({required this.points});

  @override
  void paint(Canvas canvas, Size size) {

    Paint paint = Paint()
        ..color = Colors.brown
        ..strokeCap = StrokeCap.round
        ..strokeWidth = 5.0;

    for(int i=0;i<points.length-1;i++) {
      if(points[i] != null && points[i+1] != null ) {
        canvas.drawLine(points[i]!, points[i+1]!, paint);
      }
    }

  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;

}