
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvoked: (bool didPop) {
        if(didPop) {
          return;
        }
        _showExitDialogBox(context);
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Pop Scope"),
        ),
        body: const Center(
          child: Text("Press back button"),
        ),
      ),
    );
  }

  Future _showExitDialogBox(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Are you leaving!!"),
          content: const Text("Do you want to Go back???"),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text("No"),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
                Navigator.pop(context);
              },
              child: const Text("Yes"),
            ),
          ],
        );
      },
    );
  }
}
