
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {

  late AnimationController animationcontroller;

  @override
  void initState() {
    super.initState();

    animationcontroller = AnimationController(vsync: this,duration: const Duration(seconds: 10))..repeat();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Icon"),
      ),
      body: const Center(
        child: Column(
          children: [
            Icon(
              Icons.arrow_back,
              size: 400,
              color: Colors.blue,
              fill: 0,
              // weight: 50,
              // grade: 40,
              // opticalSize: 30,
              shadows: [
                Shadow(color: Colors.yellow,blurRadius: 30,offset: Offset(8, 8)),
                // Shadow(color: Colors.red,blurRadius: 30,offset: Offset(16, 16)),
                // Shadow(color: Colors.indigo,blurRadius: 30,offset: Offset(24, 24)),
              ],
              semanticLabel: "Flutter logo",
              textDirection: TextDirection.rtl,
              applyTextScaling: false,
            ),
            FaIcon(FontAwesomeIcons.arrowRight,size: 100,textDirection: TextDirection.rtl,),
            Icon(FontAwesomeIcons.z, size: 200, color: Colors.indigo,),
          ],
        ),
      ),
    );
  }
}
