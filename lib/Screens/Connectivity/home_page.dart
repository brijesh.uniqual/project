
import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  bool isInternetOn = false;
  final Connectivity connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> streamSubscription;


  @override
  void initState() {
    super.initState();

    streamSubscription = connectivity.onConnectivityChanged
        .listen((ConnectivityResult result) {
          if(result == ConnectivityResult.mobile || result == ConnectivityResult.wifi) {
            isInternetOn = true;
            setState(() {});
          } else {
            isInternetOn = false;
            setState(() {});
          }
        });



  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Connectivity"),
      ),
      body: isInternetOn
          ? Container(
            color: Colors.green,
            child: const Center(
              child: Text("Internet is On!!!"),
            ),
          )
          : Container(
            color: Colors.redAccent,
            child: const Center(
              child: Text("Internet is Off!!!"),
            ),
          ),
    );
  }
}
