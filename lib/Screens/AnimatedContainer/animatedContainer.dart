
import 'package:flutter/material.dart';

class AnimationContainer extends StatefulWidget {
  const AnimationContainer({Key? key}) : super(key: key);

  @override
  State<AnimationContainer> createState() => _AnimationContainerState();
}

class _AnimationContainerState extends State<AnimationContainer> {
  
  var width = 100.0;
  var height = 200.0;
  bool flag = true;
  var color = Colors.green;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Animated Container"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AnimatedContainer(
              width: width,
              height: height,
              color: color,
              curve: Curves.bounceOut,
              duration: const Duration(seconds: 3),
            ),
            const SizedBox(height: 50,),
            ElevatedButton(
              onPressed: () {
                if(flag) {
                  width = 200.0;
                  height = 100.0;
                  color = Colors.red;
                } else {
                  width = 100.0;
                  height = 200.0;
                  color = Colors.green;
                }
                flag = !flag;
                setState(() {});
              }, 
              child: const Text("Do Animation"),
            ),
          ],
        ),
      ),
    );
  }
}
