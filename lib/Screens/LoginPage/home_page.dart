
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {

  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();

  late AnimationController animationController;
  late Animation<double> iconAnimation;


  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );
    iconAnimation = CurvedAnimation(
      parent: animationController,
      curve: Curves.easeInOut,
    );

    animationController..addListener(() => setState(() {}))
      ..forward();
    // animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(""),
      // ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          const Image(
            image: AssetImage('image.jpeg'),
            fit: BoxFit.fill,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.all(20),
                  child: Form(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FlutterLogo(
                            size: animationController.value * 150,
                          ),
                          const Text(
                            "Login",
                            style: TextStyle(fontSize: 50, color: Colors.white),
                          ),
                          const SizedBox(height: 16,),
                          TextFormField(
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                            decoration: const InputDecoration(
                              suffixIcon: Icon(Icons.email,color: Colors.white,),
                              labelText: "Enter Email",
                              labelStyle: TextStyle(color: Colors.white),
                            ),
                            style: const TextStyle(color: Colors.tealAccent,fontSize: 24),
                          ),
                          const SizedBox(height: 8,),
                          TextFormField(
                            controller: passController,
                            keyboardType: TextInputType.visiblePassword,
                            decoration: const InputDecoration(
                              suffixIcon: Icon(Icons.lock,color: Colors.white,),
                              labelText: "Enter Password",
                              labelStyle: TextStyle(color: Colors.white),
                            ),
                            style: const TextStyle(color: Colors.tealAccent,fontSize: 24),
                          ),
                          const SizedBox(height: 10,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                  onPressed: () {},
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      Text("Login"),
                                      SizedBox(width: 10,),
                                      Icon(Icons.login),
                                    ],
                                  )
                              ),
                            ],
                          ),
                        ],
                      )
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
