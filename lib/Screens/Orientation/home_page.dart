
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(),
      body: CustomScrollView(
        slivers: [
          const SliverAppBar(
            expandedHeight: 250,
            pinned: false,
            floating: false,
            flexibleSpace: FlexibleSpaceBar(
              background: FlutterLogo(),
              title: Text("Sliver bar"),
            ),
          ),
          SliverList(

              delegate: SliverChildBuilderDelegate(
                  childCount: 20,
                      (context, index) => ListTile(
                    title: Text("Item $index"),
                  )
              )
          ),
        ],
      ),
    );
  }
}

// OrientationBuilder(
// builder: (_,orientation) {
// return Center(
// child: Text("Data is here",style: TextStyle(color: orientation == Orientation.portrait ? Colors.red : Colors.blue),),
// );
// }
// ),


