
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        height: 300,
        width: 300,
        color: Colors.yellow,
        child: Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.bottomRight,
          children: [
            Container(
              width: 50,
              height: 50,
              color: Colors.greenAccent,
            ),
            Positioned(
              // left: 250,
              top: 20,
              right: -20,
              child: Container(
                width: 100,
                height: 100,
                color: Colors.red,
              ),
            ),
            Positioned(
              bottom: -20,
              right: -20,
              child: Container(
                width: 60,
                height: 60,
                color: Colors.black,
              ),
            )
          ],
        ),
      ),
    );
  }
}
