
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/GetX/Controller/controller.dart';
import 'package:get/get.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final Controller c = Get.put(Controller());
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Obx(()=> Text(c.string.value)),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextField(
              onChanged: (v) {
                c.changeString(v);
              },
              decoration: const InputDecoration(
                hintText: "Enter AppBar Title"
              ),
            ),
          ),
          Text(Random().nextInt(30).toString()),
          ElevatedButton(
              onPressed: () {
                Get.snackbar("GetX", "Hi, I am GetX!!!!!",snackPosition: SnackPosition.BOTTOM);
              },
              child: const Text("Show Snack Bar"),
          ),
        ],
      ),
    );
  }
}
