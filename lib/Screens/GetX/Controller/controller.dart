
import 'package:get/get.dart';

class Controller extends GetxController{
  var string = "Default Value".obs;

  changeString(String s) => string.value = s;
}