import 'package:flutter/material.dart';

const String _name = "Brijesh";

class ChatMassage extends StatelessWidget {
  const ChatMassage({Key? key, required this.textMsg}) : super(key: key);

  final String textMsg;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            CircleAvatar(
              radius: 25,
              child: Text(_name[0],style: const TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
            ),
            const SizedBox(width: 12,),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(_name,style: TextStyle(fontWeight: FontWeight.bold),),
                  const Divider(thickness: 2,),
                  Text(textMsg),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(height: 14,),
      ],
    );
  }
}
