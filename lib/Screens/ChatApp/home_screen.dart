import 'package:flutter/material.dart';

import 'chat_massage.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  TextEditingController msgController = TextEditingController();
  List<ChatMassage> massages = <ChatMassage>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Chat App"),
      ),
      body: Column(
        children: [
          Expanded(
              child: ListView.builder(
                reverse: true,
                itemCount: massages.length,
                itemBuilder: (_,index) {
                  return massages[index];
                },
              ),
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: msgController,
                    decoration: const InputDecoration.collapsed(
                      hintText: "Send massage"
                    ),
                    style: const TextStyle(fontSize: 26),
                    onFieldSubmitted: (value) => sendMassage(msgController.text),
                  ),
                ),
                IconButton(
                    onPressed: () {
                      sendMassage(msgController.text);
                    },
                    icon: const Icon(
                      Icons.send,
                      size: 26,
                      color: Colors.blue,
                    ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void sendMassage(String text) {

    if(text == "") {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Enter massage")));
    } else {
      msgController.clear();
      ChatMassage newMsg = ChatMassage(textMsg: text);

      massages.insert(0, newMsg);
      setState(() {});
    }
  }
}
