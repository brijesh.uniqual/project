
import 'dart:math';

import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  var list = List.generate(Random().nextInt(20), (index) => "Item $index ${Random().nextBool()}");
  
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Refresh list"),
      ),
      body: getBody(),
    );
  }

  getBody() {
    return RefreshIndicator(
      semanticsLabel: "Hello",
      semanticsValue: "Good",
      // displacement: 500,
      // edgeOffset: 500,
      color: Colors.yellowAccent,
        backgroundColor: Colors.black,
        onRefresh: refresh,
        child: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context,index) {
            return ListTile(
              title: Text(list[index]),
            );
          },
        )
    );
  }

  Future<void> refresh() async {

    await Future.delayed(const Duration(seconds: 2));

    list = List.generate(Random().nextInt(20), (index) => "Item $index ${Random().nextBool()}");
    setState(() {});

  }
}
