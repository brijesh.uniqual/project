
class Post{
  String name;
  String photo_url;
  String post_text;
  String post_photo_url;
  int noOfComments;
  int noOfViews;
  bool isLiked;

  Post({required this.name,required this.photo_url,required this.post_text,required this.post_photo_url,required this.noOfComments,required this.noOfViews,required this.isLiked});
}

List<Post> posts = [
  Post(name: "Brijesh", photo_url: "https://as2.ftcdn.net/v2/jpg/01/15/85/23/1000_F_115852367_E6iIYA8OxHDmRhjw7kOq4uYe4t440f14.jpg", post_text: "Hello Facebook.....", post_photo_url: "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg", noOfComments: 57, noOfViews: 508, isLiked: true),
  Post(name: "Raj", photo_url: "https://as2.ftcdn.net/v2/jpg/01/15/85/23/1000_F_115852367_E6iIYA8OxHDmRhjw7kOq4uYe4t440f14.jpg", post_text: "Hello Facebook.....", post_photo_url: "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg", noOfComments: 45, noOfViews: 508, isLiked: false),
  Post(name: "Jay", photo_url: "https://as2.ftcdn.net/v2/jpg/01/15/85/23/1000_F_115852367_E6iIYA8OxHDmRhjw7kOq4uYe4t440f14.jpg", post_text: "Hello Facebook.....", post_photo_url: "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg", noOfComments: 57, noOfViews: 0, isLiked: false),
  Post(name: "Mit", photo_url: "https://as2.ftcdn.net/v2/jpg/01/15/85/23/1000_F_115852367_E6iIYA8OxHDmRhjw7kOq4uYe4t440f14.jpg", post_text: "Hello Facebook.....", post_photo_url: "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg", noOfComments: 23, noOfViews: 7, isLiked: true),
];