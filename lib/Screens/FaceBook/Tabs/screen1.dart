
import 'dart:math';

import 'package:flutter/material.dart';

import '../Models/post_model.dart';
import '../Models/story_model.dart';

class Screen1 extends StatelessWidget {
  Screen1({Key? key}) : super(key: key);

  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: scrollController,
      child: Container(
        // height: MediaQuery.of(context).size.height,
        color: Colors.grey,
        child: Column(
          children: [
            const SizedBox(height: 1,),
            Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 2),
                child: Row(
                  children: [
                    ClipPath(
                      clipper: MyClipper(),
                      child: Image.network(
                        width: 75,
                        height: 50,
                        "https://t3.ftcdn.net/jpg/02/43/12/34/240_F_243123463_zTooub557xEWABDLk0jJklDyLSGl2jrr.jpg",
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        controller: TextEditingController(),
                        decoration:  const InputDecoration(
                            contentPadding: EdgeInsets.symmetric(horizontal: 14, vertical: 0),
                            hintText: "Write something here...",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(50))
                            )
                        ),
                      ),
                    ),
                    const SizedBox(width: 15,),
                    const Icon(Icons.photo_library_outlined, color: Colors.green,),
                    const SizedBox(width: 15,),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 8,),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 250,
              color: Colors.white,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      child: SizedBox(
                        width: 120,
                        height: 240,
                        child: Column(
                          children: [
                            Expanded(
                              child: Image.network(
                                "https://as2.ftcdn.net/v2/jpg/03/03/11/75/1000_F_303117590_NNmo6BS2fOBEmDp8uKs2maYmt03t8fSL.jpg",
                              ),
                            ),
                            const Center(child: Icon(Icons.add_circle)),
                            const SizedBox(height: 20,),
                            const Text("Create story"),
                            const SizedBox(height: 10,),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: stories.length,
                      itemBuilder: (_,index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Stack(
                            children: [
                              Card(
                                child: SizedBox(
                                  width: 120,
                                  height: 240,
                                  child: Image.network(stories[index].story_photo_url),
                                ),
                              ),
                              Column(
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: ClipPath(
                                      clipper: MyClipper(),
                                      child: Image.network(
                                        width: 75,
                                        height: 50,
                                        stories[index].photo_url,
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(stories[index].name),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            // const SizedBox(height: 8,),
            Container(
              height: MediaQuery.of(context).size.height,
              child: Expanded(
                  child: ListView.builder(
                    controller: scrollController,
                    itemCount: posts.length,
                    itemBuilder: (_,index) {
                      return Column(
                        children: [
                          const SizedBox(height: 8,),
                          Container(
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      ClipPath(
                                        clipper: MyClipper(),
                                        child: Image.network(
                                          width: 75,
                                          height: 50,
                                          posts[index].photo_url,
                                        ),
                                      ),
                                      Text(posts[index].name,style: const TextStyle(fontWeight: FontWeight.bold),),
                                      const Spacer(),
                                      const Icon(Icons.more_horiz,color: Colors.grey,),
                                      const Icon(Icons.close,color: Colors.grey,),
                                    ],
                                  ),
                                  const SizedBox(height: 6,),
                                  Row(
                                    children: [
                                      Expanded(child: Text(posts[index].post_text)),
                                    ],
                                  ),
                                  const SizedBox(height: 6,),
                                  Row(
                                    children: [
                                      SizedBox(
                                        width: MediaQuery.of(context).size.width-20,
                                        height: 400,
                                        child: Expanded(
                                          flex: 1,
                                          child: Image.network(posts[index].post_photo_url),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(height: 6,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Text("${posts[index].noOfComments} comments"),
                                      const SizedBox(width: 5,),
                                      Text("${posts[index].noOfViews} views"),
                                    ],
                                  ),
                                  const SizedBox(height: 6,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Row(
                                        children: [
                                          Icon(Icons.thumb_up,color: posts[index].isLiked ? Colors.blue : Colors.grey,),
                                          const Text("Like")
                                        ],
                                      ),
                                      Row(
                                        children: const [
                                          Icon(Icons.mark_chat_unread_outlined),
                                          Text("Comment")
                                        ],
                                      ),
                                      Row(
                                        children: const [
                                          Icon(Icons.share),
                                          Text("Share")
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.addArc(Rect.fromCircle(center: Offset(size.width/2, size.height/2), radius: size.height/2), 0, 2*pi);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;

}