
import 'package:flutter/material.dart';

import 'Tabs/screen1.dart';

class FacebookHome extends StatefulWidget{
  const FacebookHome({Key? key}) : super(key: key);

  @override
  State<FacebookHome> createState() => _FacebookHomeState();
}

class _FacebookHomeState extends State<FacebookHome> {

  
  @override
  void initState() {
    super.initState();

  }
  
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: const Text("facebook",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 34, color: Colors.blue),),
          actions: const [
            Icon(Icons.add_circle, color: Colors.black,),
            SizedBox(width: 15,),
            Icon(Icons.search, color: Colors.black,),
            SizedBox(width: 15,),
            Icon(Icons.chat, color: Colors.black,),
            SizedBox(width: 15,),
          ],
          bottom: const TabBar(
            unselectedLabelColor: Colors.grey,
            labelColor: Colors.blue,
            tabs: [
              Tab(child: Icon(Icons.home_filled, size: 32,),),
              Tab(child: Icon(Icons.airplay, size: 32,),),
              Tab(child: Icon(Icons.supervised_user_circle, size: 32,),),
              Tab(child: Icon(Icons.hot_tub_rounded, size: 32,),),
              Tab(child: Icon(Icons.notifications, size: 32,),),
              Tab(child: Icon(Icons.menu, size: 32,),),
            ],
          ),
        ),
        body: getBody(),

      ),
    );
  }

  getBody() {
    return TabBarView(
        children: [
          Screen1(),
          Screen1(),
          Screen1(),
          Screen1(),
          Screen1(),
          Screen1(),
        ],
    );
  }
}

