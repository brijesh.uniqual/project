
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Constrain Box"),
      ),
      body: Center(
        child: Column(
          children: [
            ConstrainedBox(
              constraints: const BoxConstraints(
                minHeight: 50,
                minWidth: 50,
                maxWidth: 100,
              ),
              child: ElevatedButton(onPressed: () {}, child: const Text("Overflow  Text")),
            ),
            // Container(constraints: BoxConstraints(),)
          ],
        ),
      ),
    );
  }
}
