
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../User/user_model.dart';

class DataBaseHelper {

  static final DataBaseHelper _instance = DataBaseHelper.internal();
  factory DataBaseHelper() => _instance;
  DataBaseHelper.internal();

  late final database;


  createDB() async{

    database = openDatabase(
      join(await getDatabasesPath(), 'user.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE Users(id TEXT PRIMARY KEY, username TEXT, password TEXT)',
        );
      },
      version: 1,
    );


  }

  addUser(User user) async{
    final db = await database;

    await db.insert(
      'Users',
      user.toMap(),
    );
  }

  Future<User> getUserData(String uid) async {

    final db = await database;

    final List<Map<String, Object?>> userMap = await db.query('Users');

    String? id,username,password;

    for (var element in userMap) {
      if(element['id'] == uid) {
        id = element['id'] as String;
        username = element['username'] as String;
        password = element['password'] as String;
      }
    }

    User user = User(username, password, id);

    return user;
  }

  deleteUserData(String uid) async{

    final db = await database;

    await db.delete(
      'Users',
      where: 'id = ?',

      whereArgs: [uid],
    );
  }

  getAllData() async{

    final db = await database;

    final List<Map<String, Object?>> users = await db.query('Users');

    return users;

  }

}