
class User {
  final String? id;
  final String? username;
  final String? password;

  User(this.username, this.password, this.id);

  String? get getId => id;
  String? get name => username;
  String? get pass => password;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["username"] = username;
    map["password"] = password;
    return map;
  }

}