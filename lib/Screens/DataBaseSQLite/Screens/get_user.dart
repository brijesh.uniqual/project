import 'package:flutter/material.dart';

import '../Helper/dbHelper.dart';
import '../User/user_model.dart';

class GetUser extends StatefulWidget {
  const GetUser({Key? key}) : super(key: key);

  @override
  State<GetUser> createState() => _GetUserState();
}

class _GetUserState extends State<GetUser> {

  TextEditingController idController = TextEditingController();

  final formKey = GlobalKey<FormState>();
  String? id;
  User? user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Get User"),
      ),
      body: Center(
        child: Column(
          children: [
            Form(
              key: formKey,
              child: Column(
                children: [
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator:  (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter Id!!';
                      }
                      return null;
                    },
                    controller: idController,
                    decoration: const InputDecoration(
                      labelText: "Enter user's Id",
                    ),
                    onSaved: (value) => id = value,
                  ),
                  ElevatedButton(
                      onPressed: () async{
                        final form = formKey.currentState;

                        if(form!.validate()) {
                          DataBaseHelper db = DataBaseHelper();

                          user = await db.getUserData(idController.text);
                          setState(() {});
                          print("user found");
                          if(user != null && user?.getId != null && user?.name != null && user?.pass != null) {

                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("User not found!!!!")));
                          }
                        }
                      },
                      child: const Text("Get User"))
                ],
              ),
            ),
            if(user != null && user?.getId != null && user?.name != null && user?.pass != null) ...{
              const SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text("Id "),
                      Text("User Name "),
                      Text("Password "),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("${user?.getId}"),
                      Text("${user?.name}"),
                      Text("${user?.pass}"),
                    ],
                  ),
                ],
              ),
            } else ...{
              const Text("User not found"),
            }
          ],
        ),
      ),
    );
  }
}
