

import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/DataBaseSQLite/Screens/print_all_users.dart';

import 'add_new_user.dart';
import 'delete_user.dart';
import 'get_user.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("User DataBase"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context)=> const AddNewUser()),
                  );
                },
                child: const Text("Enter New User")
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context)=> const GetUser()),
                  );
                },
                child: const Text("Get User Data")
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context)=> DeleteUser()),
                  );
                },
                child: const Text("Delete User")
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context)=> const PrintUsers()),
                  );
                },
                child: const Text("Print All Users")
            ),
            // ElevatedButton(
            //     onPressed: () {
            //       Navigator.of(context).push(
            //         MaterialPageRoute(builder: (context)=> UpdateUser()),
            //       );
            //     },
            //     child: const Text("Update User")
            // ),
          ],
        ),
      ),
    );
  }
}
