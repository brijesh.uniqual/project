import 'package:flutter/material.dart';
import '../Helper/dbHelper.dart';


class DeleteUser extends StatefulWidget {
  const DeleteUser({Key? key}) : super(key: key);

  @override
  State<DeleteUser> createState() => _DeleteUserState();
}

class _DeleteUserState extends State<DeleteUser> {

  TextEditingController idController = TextEditingController();

  final formKey = GlobalKey<FormState>();
  String? id;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Delete User"),
      ),
      body: Center(
        child: Column(
          children: [
            Form(
              key: formKey,
              child: Column(
                children: [
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator:  (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter Id!!';
                      }
                      return null;
                    },
                    controller: idController,
                    decoration: const InputDecoration(
                      labelText: "Enter user's Id",
                    ),
                    onSaved: (value) => id = value,
                  ),
                  ElevatedButton(
                      onPressed: () async{
                        final form = formKey.currentState;

                        if(form!.validate()) {
                          DataBaseHelper db = DataBaseHelper();

                          await db.deleteUserData(idController.text);

                          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("User Deleted from the DataBase")));

                          Navigator.pop(context);

                          print("user deleted");
                        }
                      },
                      child: const Text("Delete User"))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
