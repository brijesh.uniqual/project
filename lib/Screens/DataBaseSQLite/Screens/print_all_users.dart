
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/DataBaseSQLite/Helper/dbHelper.dart';

import '../User/user_model.dart';

class PrintUsers extends StatefulWidget {
  const PrintUsers({Key? key}) : super(key: key);

  @override
  State<PrintUsers> createState() => _PrintUsersState();
}

class _PrintUsersState extends State<PrintUsers> {

  List<dynamic> users = [];

  Future loadData() async{
    final db = DataBaseHelper();

    users = await db.getAllData();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    loadData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("All Users"),
      ),
      body: users.length == 0
          ? const Text("Users Data Not Found!!!!")
          : ListView.builder(
              itemCount: users.length,
              itemBuilder: (_,index) {
                return ListTile(
                  leading: Text("${users[index]["id"]}"),
                  title: Text("${users[index]["username"]}"),
                  subtitle: Text("${users[index]["password"]}"),
                );
              }
            ),
    );
  }
}
