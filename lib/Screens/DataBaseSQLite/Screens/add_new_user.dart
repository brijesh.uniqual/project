
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/DataBaseSQLite/Helper/dbHelper.dart';
import 'package:flutter_app/Screens/DataBaseSQLite/User/user_model.dart';

class AddNewUser extends StatefulWidget {
  const AddNewUser({Key? key}) : super(key: key);

  @override
  State<AddNewUser> createState() => _AddNewUserState();
}

class _AddNewUserState extends State<AddNewUser> {

  TextEditingController idController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController passController = TextEditingController();

  final formKey = GlobalKey<FormState>();
  String? id,userName,password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add New User"),
      ),
      body: Center(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator:  (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter Id!!';
                  }
                  return null;
                  },
                controller: idController,
                decoration: const InputDecoration(
                  labelText: "Id",
                ),
                onSaved: (value) => id = value,
              ),
              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator:  (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter User name!!';
                  }
                  return null;
                },
                controller: nameController,
                decoration: const InputDecoration(
                  labelText: "User Name",
                ),
                onSaved: (value) => userName = value,
              ),
              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator:  (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter Password!!';
                  }
                  return null;
                },
                controller: passController,
                decoration: const InputDecoration(
                  labelText: "Password",
                ),
                onSaved: (value) => password = value,
              ),
              ElevatedButton(
                  onPressed: () async{
                    final form = formKey.currentState;

                    if(form!.validate()) {
                      DataBaseHelper db = DataBaseHelper();
                      User user = User(nameController.text, passController.text,idController.text);

                      try {
                        await db.addUser(user);

                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("User Added to DataBase")));

                        Navigator.pop(context);

                        print("User added");
                      } catch(DatabaseException ) {
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("User Already exist into DataBase")));
                        print("errorrrrr");
                      }
                    }
                  },
                  child: const Text("Add User"))
            ],
          ),
        ),
      ),
    );
  }
}
