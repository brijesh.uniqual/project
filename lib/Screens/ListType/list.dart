import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers.dart';

List<String> list = List.generate(5, (i) => "LIST ${i+1}");

class ExpendableList extends StatelessWidget {
  const ExpendableList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: RefreshIndicator(
        onRefresh: () async{
          refreshlist();
        },
        child: ListView.builder(
          itemCount: 10,
          itemBuilder: (context,i) {
            return ExpansionTile(

              // maintainState: true,
              // expandedAlignment: Alignment.centerRight,
              // // expandedCrossAxisAlignment: CrossAxisAlignment.center,
              leading: const Icon(Icons.add),
              title: Text("Header ${i+1}"),
              children: list.map(
                      (e) => ListTile(
                        title: Text(e),
                      ),
              ).toList(),
              /*const [
                Text("data"),
                Text("data"),
                Text("data"),
                Text("data"),
                Text("data"),
                Text("data"),

              ],*/
            );
          },
        ),
      ),
    );
  }

  Future<void> refreshlist() async {

    // await Future.delayed(Duration(seconds: 2));
    list = List.generate(10, (i) => "LIST ${i+1}");
    print("refresh");

  }
}

class StickyList extends StatelessWidget {
  const StickyList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
        itemCount: 6,
        itemBuilder: (context,i) {
          return StickyHeader(
            overlapHeaders: false,
            header: Container(
                color: Colors.green,
                child: Text("Heading ${i+1}",style: TextStyle(fontSize: 26,fontWeight: FontWeight.bold),)),
            content: Column(
              children: list.map(
                      (e) => ListTile(
                        title: Text("Data ${i+1}"),
                      ),
              ).toList(),
            ),

          );
        },
      ),
    );
  }
}

