
import 'package:backdrop/backdrop.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/ListType/list.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void didChangeDependencies() {
    print("didChangeDependencies");
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BackdropScaffold(
      subHeader: const Center(child: Text("Front layers Heading")),
      appBar: BackdropAppBar(
        title: const Text("Title"),
      ),
      backLayer: const Center(
        child: Text("Back Side"),
      ),
      frontLayer: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const ExpendableList()));
              },
              child: const Text("Expendable List"),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const StickyList()));
              },
              child: const Text("Expendable List"),
            ),
          ],
        ),
      ),
    );
  }
}
