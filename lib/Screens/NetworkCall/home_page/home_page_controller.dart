
import 'dart:convert';

import 'package:flutter_app/Screens/NetworkCall/home_page/home_page_user_model.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class HomePageController extends GetxController {

  List<UserModel> users = [];

  String url = "https://api.github.com/users";
  bool isLoading = false;


  @override
  void onInit() {
    super.onInit();

    getUserData();

    update();
  }

  void getUserData() async{

    isLoading = true;

    update();

    http.Response response = await http.get(Uri.parse(url));

    if(response.statusCode  == 200) {

      final responseObj = json.decode(response.body);

      users = List<UserModel>.from((responseObj.map((value) => UserModel.fromJson(value))));
    }

    isLoading = false;
    update();

  }


}