
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/Screens/NetworkCall/home_page/home_page_controller.dart';
import 'package:get/get.dart';

class HomePageView extends StatelessWidget {
  const HomePageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomePageController>(
      init: HomePageController(),
      dispose: (_) => Get.delete<HomePageController>(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("API call"),
          ),
          body: _.isLoading
              ? const Center(child: CircularProgressIndicator())
              : getBody(_),
        );
      }
    );
  }

  Widget getBody(HomePageController _) {
    return ListView.builder(
      // scrollDirection: Axis.horizontal,
      itemCount: _.users.length,
      itemBuilder: (context,index) {
        return ListTile(
          leading: CircleAvatar(
            child: Image.network(_.users[index].avatarUrl!),
          ),
          title: Text(_.users[index].login!),
          subtitle: Text("User Id: ${_.users[index].nodeId!}"),
        );
      },
    );
  }
}
