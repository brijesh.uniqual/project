// import 'dart:convert';
//
// import 'package:flutter/material.dart';
// import 'package:http/http.dart';
//
//
// class HomePage extends StatefulWidget {
//   const HomePage({super.key});
//
//   @override
//   State<HomePage> createState() => _HomePageState();
// }
//
// class _HomePageState extends State<HomePage> {
//
//   List users = [];
//   bool _isLoading = false;
//
//   @override
//   void initState() {
//     super.initState();
//     getUserData();
//
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text("API call"),
//       ),
//       body: _isLoading
//           ? const Center(
//               child: CircularProgressIndicator(),
//             )
//           : getBody(),
//     );
//   }
//
//   Widget getBody() {
//     return ListView.builder(
//       itemCount: users.length,
//       itemBuilder: (_,index) {
//         return ListTile(
//           leading: CircleAvatar(
//             child: Image.network(users[index]["avatar_url"]),
//           ),
//           title: Text(users[index]["login"]),
//           subtitle: Text("User Id: " + users[index]["node_id"]),
//         );
//       },
//     );
//   }
//
//
//
//   getUserData() async{
//     String url = "https://api.github.com/users";
//     setState(() {
//       _isLoading = true;
//     });
//     Response response = await get(Uri.parse(url));
//
//     users = json.decode(response.body);
//
//     setState(() {
//       _isLoading = false;
//     });
//   }
//
// }
