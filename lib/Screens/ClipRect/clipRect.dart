
import 'package:flutter/material.dart';

class ClipRectWidget extends StatelessWidget {
  const ClipRectWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Clip"),
      ),
      body: Center(
        child: ClipOval(
          child: Container(
            width: 200,
            height: 100,
            // color: Colors.blueGrey,
            // transform: Matrix4.,
            decoration: const BoxDecoration(
              gradient: RadialGradient(
                colors: [
                  Colors.black,Colors.black12
                ],
                // stops: [0.1,1],
                // focalRadius: 50
              ),
            ),
          ),
        ),
      ),
    );
  }
}
