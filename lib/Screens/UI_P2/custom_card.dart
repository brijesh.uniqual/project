
import 'package:flutter/material.dart';

class CustomCard extends StatefulWidget {
  CustomCard({super.key, required this.photo_url, required this.icon, required this.number, required this.name});

  String photo_url;
  IconData icon;
  int number;
  String name;

  @override
  State<CustomCard> createState() => _CustomCardState();
}

class _CustomCardState extends State<CustomCard> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(35),
          image: DecorationImage(
            colorFilter: ColorFilter.mode(Colors.purpleAccent.withOpacity(0.3), BlendMode.difference),
            fit: BoxFit.cover,
            image: NetworkImage(widget.photo_url),
          )
        ),
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(widget.icon,size: 45,),
              const Spacer(),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FittedBox(child: Text("${widget.name}", style: const TextStyle(color: Colors.white,fontSize: 28),)),
                        Text("${widget.number} SPOTS", style: const TextStyle(color: Colors.white,fontSize: 12),),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
