
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/UI_P2/custom_card.dart';


class BTab1 extends StatelessWidget {
  const BTab1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 2),
              color: Colors.black54,
              child: const TabBar(
                tabs: [
                  Tab(child: Text("DISCOVER")),
                  Tab(child: Text("ACTIVITIES")),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                children: [
                  Container(
                    color: Colors.black,
                    child: Column(
                      children: [
                        Expanded(
                          flex: 1,
                          child: CustomCard(photo_url: "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg", icon: Icons.account_tree, number: 234, name: "Nature's Light"),
                        ),
                        Expanded(
                          flex: 3,
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: CustomCard(photo_url: "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg", icon: Icons.cell_tower, number: 450, name: "Cultural"),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: CustomCard(photo_url: "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg", icon: Icons.sunny, number: 347, name: "Popularity"),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: CustomCard(photo_url: "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg", icon: Icons.factory, number: 234, name: "Modern Life"),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: CustomCard(photo_url: "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg", icon: Icons.face, number: 146, name: "Sun & Sand"),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.blue,
                  )
                ],
              ),
            ),
          ],
        ),
      ),

    );
  }
}
