
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/UI_P2/bTab1.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {

  late TabController topTabController;
  int selectedIndex = 0;

  static const List<Widget> bottomBarOptions = <Widget>[
    BTab1(),
    Text("Compass"),
    Text("BookMarks"),
    Text("Chats"),
    Text("User"),
  ];

  @override
  void initState() {
    super.initState();
    topTabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          title: const Text("Explore"),
          actions: const [
            Icon(Icons.notifications_none_rounded),
            SizedBox(width: 10,),
            Icon(Icons.search),
            SizedBox(width: 20,),
          ],
        ),
        drawer: const Drawer(),
        body: getBody(),
        bottomNavigationBar: BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.travel_explore, size: 40,),
              label: "Travel",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.assistant_navigation, size: 40,),
              label: "Compass",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.bookmark_added_rounded, size: 40,),
              label: "Bookmark",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.chat, size: 40,),
              label: "Chats",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_pin, size: 40,),
              label: "Profile",
            ),
          ],
          currentIndex: selectedIndex,
          onTap: onItemTapped,
        ),
      ),
    );
  }


  getBody() {
    return Center(
      child: bottomBarOptions.elementAt(selectedIndex),
    );
  }

  void onItemTapped(int value) {
    setState(() {
      selectedIndex = value;
    });
  }
}
