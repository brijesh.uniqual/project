
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Clipping with Photo"),
      ),
      body: Center(
        child: ClipPath(
          clipper: MyClipper(),
          child: Image.network("https://buffer.com/cdn-cgi/image/w=1000,fit=contain,q=90,f=auto/library/content/images/size/w1200/2023/09/instagram-image-size.jpg"),
        ),
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    // path.addArc(Rect.fromCircle(center: Offset(size.width/2, size.height/2), radius: size.height/2), 0, 2*pi);
    // // path.moveTo(size.width, size.height);
    // path.lineTo(0.0, size.height);
    // path.lineTo(size.width, 0.0);
    // path.lineTo(size.width, size.height);

    path.moveTo(25, 25);

    var Cpoint1 = Offset(0.0, size.height/4);
    var Epoint1 = Offset(25, size.height/2);

    path.quadraticBezierTo(Cpoint1.dx, Cpoint1.dy, Epoint1.dx, Epoint1.dy);

    var Cpoint2 = Offset(50.0, 3*size.height/4);
    var Epoint2 = Offset(25.0, size.height-25);

    path.quadraticBezierTo(Cpoint2.dx, Cpoint2.dy, Epoint2.dx, Epoint2.dy);

    var firstCpoint = Offset(size.width/4, size.height);
    var firstEpoint = Offset(size.width/2, size.height-25);

    path.quadraticBezierTo(firstCpoint.dx, firstCpoint.dy, firstEpoint.dx, firstEpoint.dy);

    var SecondCpoint = Offset(3*size.width/4, size.height-50);
    var SecondEpoint = Offset(size.width-25, size.height-25);

    path.quadraticBezierTo(SecondCpoint.dx, SecondCpoint.dy, SecondEpoint.dx, SecondEpoint.dy);

    var Cpoint3 = Offset(size.width, 3*size.height/4);
    var Epoint3 = Offset(size.width-25.0, size.height/2);

    path.quadraticBezierTo(Cpoint3.dx, Cpoint3.dy, Epoint3.dx, Epoint3.dy);


    var Cpoint4 = Offset(size.width-50.0, size.height/4);
    var Epoint4 = Offset(size.width-25.0, 25.0);

    path.quadraticBezierTo(Cpoint4.dx, Cpoint4.dy, Epoint4.dx, Epoint4.dy);

    var Cpoint5 = Offset(3*size.width/4, 0.0);
    var Epoint5 = Offset(size.width/2, 25.0);

    path.quadraticBezierTo(Cpoint5.dx, Cpoint5.dy, Epoint5.dx, Epoint5.dy);

    var Cpoint6 = Offset(size.width/4, 50.0);
    var Epoint6 = const Offset(25.0, 25.0);

    path.quadraticBezierTo(Cpoint6.dx, Cpoint6.dy, Epoint6.dx, Epoint6.dy);
    path.lineTo(size.width, 0.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;

}



