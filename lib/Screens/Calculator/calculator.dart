import 'package:flutter/material.dart';

class Calculator extends StatefulWidget {
  const Calculator({Key? key}) : super(key: key);

  @override
  State<Calculator> createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {

  TextEditingController num1Controller = TextEditingController(text: "0");
  TextEditingController num2Controller = TextEditingController(text: "0");
  double output = 0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Calculator",style: TextStyle(fontSize: 28),),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(35.0),
            child: Column(
              children: [
                Text("Output: ${output.toStringAsFixed(2)}", style: const TextStyle(fontSize: 26, fontWeight: FontWeight.bold),),
                const SizedBox(height: 14,),
                TextFormField(
                  controller: num1Controller,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                    labelText: "Enter Number 1"
                  ),
                ),
                const SizedBox(height: 10,),
                TextFormField(
                  controller: num2Controller,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                      labelText: "Enter Number 2"
                  ),
                ),
                const SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(onPressed: () {
                      setState(() {
                        output =  double.parse(num1Controller.text) + double.parse(num2Controller.text);
                      });
                    }, child: const Text("+")),
                    ElevatedButton(onPressed: () {
                      setState(() {
                        output =  double.parse(num1Controller.text) - double.parse(num2Controller.text);
                      });
                    }, child: const Text("-")),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(onPressed: () {
                      setState(() {
                        output =  double.parse(num1Controller.text) * double.parse(num2Controller.text);
                      });
                    }, child: const Text("*")),
                    ElevatedButton(onPressed: () {
                      if(!(double.parse(num2Controller.text) == 0.0)) {
                        setState(() {
                          output =  double.parse(num1Controller.text) / double.parse(num2Controller.text);
                        });
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text("Can't divide by zero(0).")
                            )
                        );
                      }
                    }, child: const Text("/")),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
