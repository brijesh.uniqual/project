
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/ShoppingCartRedux/add_item_dialog.dart';
import 'package:flutter_redux_dev_tools/flutter_redux_dev_tools.dart';


import 'model/cart_item.dart';
import 'shopping_list.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';


class ShoppingCart extends StatefulWidget {

  final DevToolsStore<List<CartItem>> store;

  const ShoppingCart({Key? key, required this.store}) : super(key: key);

  @override
  State<ShoppingCart> createState() => _ShoppingCartState();
}

class _ShoppingCartState extends State<ShoppingCart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Shopping Cart"),
      ),
      body: const ShoppingList(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showAddDialog(context);
        },
        child: const Icon(Icons.add_circle),
      ),
      endDrawer: ReduxDevTools(widget.store),
    );
  }
}

_showAddDialog(BuildContext context) {
  showDialog(context: context, builder: (context) => const AddDialog());
}