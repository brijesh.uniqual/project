
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/ShoppingCartRedux/model/cart_item.dart';
import 'package:flutter_app/Screens/ShoppingCartRedux/redux/actions.dart';
import 'package:flutter_redux/flutter_redux.dart';


class ShoppingItem extends StatelessWidget {

  final CartItem item;
  const ShoppingItem({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<List<CartItem>,OnDeleteItemCallBack>(
      converter: (store) => (i) => store.dispatch(DeleteAction(item)),
      builder: (context,callBack) {
        return Dismissible(
          onDismissed: (value) {
            callBack(item);
          },
          key: Key(item.name),
          child: StoreConnector<List<CartItem>,OnToggleItemCallBack>(
            converter: (store) => (item) => store.dispatch(ToggleStateAction(item)),
            builder: (context, callBack) {
              return ListTile(
                leading: Checkbox(
                  value: item.checked,
                  onChanged: (value) {
                    callBack(CartItem(name: item.name, checked: value!));
                  },
                ),
                title: Text(item.name,style: const TextStyle(fontSize: 22),),
              );
            },
          ),
        );
      },
    );
  }
}

typedef OnToggleItemCallBack = Function(CartItem item);
typedef OnDeleteItemCallBack = Function(CartItem item);