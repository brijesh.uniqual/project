


import 'package:flutter_app/Screens/ShoppingCartRedux/model/cart_item.dart';

class AddItemActions {
  final CartItem item;

  AddItemActions(this.item);
}

class ToggleStateAction {
  final CartItem item;

  ToggleStateAction(this.item);
}

class DeleteAction {
  final CartItem item;

  DeleteAction(this.item);
}