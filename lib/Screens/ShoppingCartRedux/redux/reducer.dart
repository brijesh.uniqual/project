


import 'package:flutter_app/Screens/ShoppingCartRedux/redux/actions.dart';

import '../model/cart_item.dart';

List<CartItem> cartItemReducer(List<CartItem> items, dynamic action) {

  if(action is AddItemActions) {
    return addItem(items, action);
  } else if(action is ToggleStateAction) {
    return toggleState(items, action);
  } else if(action is DeleteAction) {
    return deleteItem(items, action);
  }

  return items;

}

List<CartItem> addItem(List<CartItem> item, AddItemActions actions) {
  return List.from(item)..add(actions.item);
}

List<CartItem> toggleState(List<CartItem> item, ToggleStateAction actions) {
  List<CartItem> newList = item.map((item) => item.name == actions.item.name ? actions.item : item).toList();
  return newList;
}

List<CartItem> deleteItem(List<CartItem> item, DeleteAction actions) {
  return List.from(item)..remove(actions.item);
}
