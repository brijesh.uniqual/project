
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/ShoppingCartRedux/model/cart_item.dart';
import 'package:flutter_app/Screens/ShoppingCartRedux/redux/actions.dart';
import 'package:flutter_redux/flutter_redux.dart';


class AddDialog extends StatefulWidget {
  const AddDialog({Key? key}) : super(key: key);

  @override
  State<AddDialog> createState() => _AddDialogState();
}

class _AddDialogState extends State<AddDialog> {

  String itemName = "";

  @override
  Widget build(BuildContext context) {
    return StoreConnector<List<CartItem>,OnItemAddedCallBack>(
      converter: (store) => (itemName) => store.dispatch(AddItemActions(CartItem(name: itemName, checked: false))),
      builder: (context, callBack) {
        return AlertDialog(
          title: const Text("Add Item to Cart"),
          content: TextField(
            autofocus: true,
            decoration: const InputDecoration(
                hintText: "Cart Item....."
            ),
            onChanged: (value) {
              setState(() {
                itemName = value;
              });
            },
          ),
          actions: [
            ElevatedButton(
              onPressed: (){
                Navigator.pop(context);
              },
              child: const Text("Cancel"),
            ),
            ElevatedButton(
              onPressed: (){
                // print("Item added..");
                callBack(itemName);
                Navigator.pop(context);
              },
              child: const Text("OK"),
            ),
          ],
        );
      },
    );
  }
}

typedef OnItemAddedCallBack = Function(String itemName);