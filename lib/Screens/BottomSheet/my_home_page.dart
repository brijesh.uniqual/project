
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key}) : super(key: key);

  final sKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: sKey,
      appBar: AppBar(
        title: const Text("Bottom Sheet"),
      ),
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
                onPressed: () {
                  sKey.currentState?.showBottomSheet((context) =>
                      Container(
                        height: 300,
                        color: Colors.blue,
                      ),
                  );

                },
                child: const Text("Persi..Sheet")
            ),
            ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Container(
                          color: Colors.blue,
                        );
                      });
                },
                child: const Text("Model..Sheet")
            ),
          ],
        ),
      ),
    );
  }

}


