
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_screen.dart';
import 'home_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

    Timer(const Duration(seconds: 2), () {checkLogin();});

  }

  @override
  Widget build(BuildContext context) {

    return Container(
      color: Colors.amber.shade50,
      child: const FlutterLogo(),
    );
  }

  void checkLogin() async{

    var pref = await SharedPreferences.getInstance();

    bool? isLogin = pref.getBool("isLogin");
    
    if(isLogin != null) {
      if(isLogin) {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomeScreen()));
      } else {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen()));
      }
    } else {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen()));
    }

  }
}
