
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/SharedPref/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  String email = "";
  String pass = "";

  @override
  void initState() {
    super.initState();

    getEmailAndPass();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home Page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(height: 50,),
            Text(email,style: const TextStyle(fontSize: 28),),
            Text(pass,style: const TextStyle(fontSize: 28),),
            const Spacer(),
            ElevatedButton(
              onPressed: () {
                logOut();
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen()));
              },
              child: const Text("LogOut"),
            ),
            const SizedBox(height: 50,),

          ],
        ),
      ),
    );
  }

  void getEmailAndPass() async{


    var pref = await SharedPreferences.getInstance();

    email = pref.getString("login") ?? "No Found!!!";
    pass = pref.getString("pass") ?? "No Data!!!";

    setState(() {});
  }

  void logOut() async{

    var pref = await SharedPreferences.getInstance();

    pref.setBool("isLogin",false);

  }

}
