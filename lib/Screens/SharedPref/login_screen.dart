
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();

  TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                controller: emailController,
                decoration: const InputDecoration(
                  label: Text("Email")
                ),
              ),
              TextField(
                controller: passController,
                decoration: const InputDecoration(
                    label: Text("Password")
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  checkEmailAndPass(context);
                },
                child: const Text("Login"),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void checkEmailAndPass(BuildContext context) async{

    var pref = await SharedPreferences.getInstance();

    if(pref.getString("login") == emailController.text.toString() && pref.getString("pass") == passController.text.toString()) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomeScreen()));
    } else if(pref.getString("login") == null && pref.getString("pass") == null) {
      pref.setString("login", emailController.text.toString());
      pref.setBool("isLogin", true);
      pref.setString("pass", passController.text.toString());

      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomeScreen()));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Please Check Email or Password!!!")));
    }

  }
}
