
import 'dart:math';

import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {

  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin{

  late AnimationController forwardController;
  late AnimationController backwardController;

  @override
  void initState() {
    super.initState();

    forwardController = AnimationController(value: 20,vsync: this, duration: const Duration(seconds: 10),animationBehavior: AnimationBehavior.preserve)..repeat();
    backwardController = AnimationController(vsync: this, duration: const Duration(seconds: 50))..repeat();

  }

  @override
  void dispose() {

    forwardController.dispose();
    backwardController.dispose();

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Animation-2"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AnimatedBuilder(
              builder: (context, child) {
                return Transform.rotate(
                  angle: forwardController.value * pi * 1.0,
                  child: child,
                );
              },
              animation: forwardController,
              child: Container(
                color: Colors.red,
                height: 100,
                width: 100,
              ),
            ),
            const SizedBox(height: 20,),
            Text(Random().nextInt(30).toString()),
            const SizedBox(height: 20,),
            AnimatedBuilder(
              builder: (context, child) {
                return Transform.rotate(
                  filterQuality: FilterQuality.low,
                  angle: backwardController.value * pi * -10.0,
                  child: child,
                );
              },
              animation: backwardController,
              child: Container(
                color: Colors.blueGrey,
                height: 100,
                width: 100,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
