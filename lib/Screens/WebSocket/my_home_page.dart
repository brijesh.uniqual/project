import 'dart:io';

import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
// import 'package:web_socket_channel/status.dart' as status;


class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController txtController = TextEditingController();

  late final _channel;
  final _channel1 = WebSocketChannel.connect(
    Uri.parse('wss://echo.websocket.events'),
  );

  @override
  void initState() {
    connectWebSocket();
    super.initState();
  }

  @override
  void dispose() {
    _channel.sink.close();
    txtController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text("Web Socket"),
      ),
      body: Column(
        children: [
          Form(
            child: TextFormField(
              controller: txtController,
              onTapOutside: (v) {
                FocusManager.instance.primaryFocus?.unfocus();
              },
              decoration: const InputDecoration(labelText: "Enter Massage"),
            ),
          ),
          StreamBuilder(
            stream: _channel.stream,
            builder: (BuildContext context, AsyncSnapshot<dynamic> snap) {
              return Text("Data: ${snap.hasData ? snap.data : ""}");
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (txtController.text.trim().isNotEmpty) {

            _channel.sink.add(txtController.text.trim());
            print('data send');
          } else {
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Please Enter Msg...!!!")));
          }
        },
      ),
    );
  }


  connectWebSocket() async {
    try {
      _channel = WebSocketChannel.connect(
        Uri.parse('wss://echo.websocket.events'),
      );
      await _channel.ready;
    } on SocketException catch (e) {
      print(e.toString());
    } on WebSocketChannelException catch (e) {
      print(e.toString());
    }
  }
}
