
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ListWheel extends StatelessWidget {
  ListWheel({Key? key}) : super(key: key);

  var data = [1,2,3,4,5,6,7,8,9,10];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("List Wheel View"),),
      body: ListWheelScrollView(
        // useMagnifier: true,
        // magnification: 1.5,
        physics: BouncingScrollPhysics(),
        // overAndUnderCenterOpacity: 0.5,
        // squeeze: 1,
        clipBehavior: Clip.none,
        renderChildrenOutsideViewport: true,
        restorationId: "3",
        // offAxisFraction: 1,
        // perspective: ,
        diameterRatio: 3.5,
        itemExtent: 200,
        children: data.map((e) => Container(
          width: MediaQuery.of(context).size.width-20,
          decoration: BoxDecoration(
            color: Colors.blueGrey,
            borderRadius: BorderRadius.circular(45),
          ),
          child: Center(child: Text('$e',style: const TextStyle(color: Colors.white,fontSize: 26),)),
        )).toList(),
      ),
    );
  }
}
