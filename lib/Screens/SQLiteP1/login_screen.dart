
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/SQLiteP1/DBHelper/db_helper.dart';
import 'package:flutter_app/Screens/SQLiteP1/register_screen.dart';
import 'package:flutter_app/Screens/SQLiteP1/home_screen.dart';


import 'Model/user_model.dart';
import 'all_user_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Login"),
      ),
      body: Center(
        child: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Login"),
                const SizedBox(height: 30,),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if(!(value!.contains("@"))) {
                      return "Please enter email properly";
                    }
                    return null;
                  },
                  controller: emailController,
                  decoration: const InputDecoration(
                    hintText: "Enter Email"
                  ),
                ),
                const SizedBox(height: 20,),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if((value!.length < 6)) {
                      return "Please enter pass greater than 6";
                    }
                    return null;
                  },
                  controller: passController,
                  decoration: const InputDecoration(
                      hintText: "Enter Password"
                  ),
                ),
                const SizedBox(height: 20,),
                ElevatedButton(
                    onPressed: () {
                      login();
                    },
                    child: const Text("Login"),
                ),
                const SizedBox(height: 20,),
                ElevatedButton(
                    onPressed: () {
                      register();
                    },
                    child: const Text("Register"),
                ),
                const SizedBox(height: 20,),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const AllUserScreen()));
                  },
                  child: const Text("Get All User"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void login() async{

    if(formKey.currentState!.validate()) {

      String email = emailController.text;
      String pass = passController.text;

      DatabaseHelper db = DatabaseHelper();

      bool isExist = await db.checkForUser(email , pass);

      if(isExist) {

        bool isValidPass = await db.checkPassword(email, pass);

        if(isValidPass) {
          User? user = await db.getUser(email);

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomeScreen(user: user),
            ),

          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Password is Wrong!!!!!\nPlease Check your password..!")));
        }

      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("User not Found!!!!\nPlease Register first...")));
      }

    }





  }

  void register() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => const RegisterScreen()),
    );
  }
}
