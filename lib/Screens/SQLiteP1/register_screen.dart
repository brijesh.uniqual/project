
import 'package:flutter/material.dart';
import 'package:flutter_app/Screens/SQLiteP1/DBHelper/db_helper.dart';
import 'package:flutter_app/Screens/SQLiteP1/Model/user_model.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register"),
      ),
      body: Center(
        child: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Register New User",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
                const SizedBox(height: 50,),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if(value!.isEmpty) {
                      return "Please enter User Name";
                    }
                    return null;
                  },
                  controller: nameController,
                  decoration: const InputDecoration(
                      hintText: "Enter User Name"
                  ),
                ),
                const SizedBox(height: 20,),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if(!(value!.contains("@"))) {
                      return "Please enter email properly";
                    }
                    return null;
                  },
                  controller: emailController,
                  decoration: const InputDecoration(
                      hintText: "Enter Email"
                  ),
                ),
                const SizedBox(height: 20,),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if((value!.length < 6)) {
                      return "Please enter pass greater than 6";
                    }
                    return null;
                  },
                  controller: passController,
                  decoration: const InputDecoration(
                      hintText: "Enter Password"
                  ),
                ),
                const SizedBox(height: 20,),
                ElevatedButton(
                    onPressed: () {
                      register();
                      Navigator.pop(context);
                    },
                    child: const Text("Register"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void register() async{
    if(formKey.currentState!.validate()) {


      String id = UniqueKey().toString();
      String name = nameController.text;
      String email = emailController.text;
      String password = passController.text;

      User user = User(id: id, name: name, email: email, password: password);

      DatabaseHelper db = DatabaseHelper();

      await db.addUser(user);

    }
  }
}
