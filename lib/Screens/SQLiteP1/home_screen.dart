
import 'package:flutter/material.dart';

import 'DBHelper/db_helper.dart';
import 'Model/user_model.dart';
import 'login_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key, required this.user}) : super(key: key);

  final User? user;

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("User Data"),
      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 200,),
              Text("User Name:  ${widget.user!.name}"),
              Text("User Id : ${widget.user!.id}"),
              Text("User Email: ${widget.user!.email}"),
              Text("User Password: ${widget.user!.password}"),
              const Spacer(),
              ElevatedButton(
                  onPressed: () async{

                    DatabaseHelper db = DatabaseHelper();

                    await db.deleteUser(widget.user!.id);

                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen()));
                  },
                  child: const Text("Delete User"),
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen()));
                  },
                  child: const Text("Log Out"),
              ),
              const SizedBox(height: 150,),
            ],
          ),
      ),
    );
  }
}
