
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../Model/user_model.dart';

class DatabaseHelper {


  late Database dataBase;

  static final DatabaseHelper _instance = DatabaseHelper.internals();
  factory DatabaseHelper() => _instance;

  DatabaseHelper.internals();

  createDBUser() async{

    dataBase = await openDatabase(
      join(getDatabasesPath().toString(), "user.db"),
      onCreate: (database,version){
        return database.execute("CREATE TABLE USER (id TEXT PRIMARY KEY, name TEXT, email TEXT, password TEXT)");
      },
      version: 1
    );
  }


  addUser(User user) async {
    final db = dataBase;

    db.insert(
      "User",
      user.toMap()
    );
  }

  Future<User?> getUser(String email) async{
    final db = dataBase;

    var usermap = await db.query("USER");


    List<User> user = [];

    for(var map in usermap){
      user.add(User.fromJson(map));
    }

    for(User u in user) {
      if(u.email == email) {
        return u;
      }
    }

    return null;
  }

  Future<List<User?>> getAllUser() async{
    final db = dataBase;

    var usermap = await db.query("USER");


    List<User> user = [];

    for(var map in usermap){
      user.add(User.fromJson(map));
    }


    return user;
  }

  deleteUser(String? id) {
    final db = dataBase;

    db.delete(
        "USER",
      where: "id = ?",
      whereArgs: [id]
    );

  }

  Future<bool> checkPassword(String email, String pass) async{

    final db = dataBase;

    var users = await db.query("USER");

    List<User> userList = [];

    for(var u in users) {
      userList.add(User.fromJson(u));
    }

    for(User data in userList) {
      if(data.email == email && data.password == pass) {

        return true;
      }
    }
    return false;

  }


  Future<bool> checkForUser(String email, String pass) async{

    final db = dataBase;

    var users = await db.query("USER");


    List<User> userList = [];

    for(var u in users) {
      userList.add(User.fromJson(u));
    }

    for(User data in userList) {
      if(data.email == email /*&& data.password == pass*/) {

        return true;
      }
    }
    return false;
  }
}