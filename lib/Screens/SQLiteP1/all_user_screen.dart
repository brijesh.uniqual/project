
import 'package:flutter/material.dart';

import 'DBHelper/db_helper.dart';
import 'Model/user_model.dart';

class AllUserScreen extends StatefulWidget {
  const AllUserScreen({Key? key}) : super(key: key);

  @override
  State<AllUserScreen> createState() => _AllUserScreenState();
}

class _AllUserScreenState extends State<AllUserScreen> {

  List<User?> users = [];
  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    getAllUserData();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.blue,
      appBar: AppBar(
        title: const Text("All User Screen"),
      ),
      body: isLoading
          ? const Center(child: CircularProgressIndicator())
          : getBody(),
    );
  }

  void getAllUserData() async{

    DatabaseHelper db = DatabaseHelper();

    setState(() {
      isLoading = true;
    });

    users = await db.getAllUser();

    setState(() {
      isLoading = false;
    });

  }

  getBody() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Container(
            color: Colors.grey,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("All User Data",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
                ),
              ],
            ),
          ),
          Expanded(
            child: GridView.builder(
              // physics: BouncingScrollPhysics().applyTo(ClampingScrollPhysics()),
              // shrinkWrap: true,
              // reverse: true,
              itemCount: users.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: users.length%3 == 0 ? 3 : 2,
                // childAspectRatio: 2,
                // mainAxisExtent: 100,
                // mainAxisSpacing: 20,
                // crossAxisSpacing: 30,
              ),
              itemBuilder: (context,index) {
                return Card(
                  clipBehavior: Clip.hardEdge,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Id: ${users[index]?.id}"),
                          Text("Name: ${users[index]?.name}"),
                          Text("Email: ${users[index]?.email}"),
                          Expanded(
                            child: Text(
                              "Password: ${users[index]?.password}Password: ${users[index]?.password}Password: ${users[index]?.password}Password: ${users[index]?.password}Password: ${users[index]?.password}Password: ${users[index]?.password}Password: ${users[index]?.password}Password: ${users[index]?.password}Password: ${users[index]?.password}brijesh",
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
