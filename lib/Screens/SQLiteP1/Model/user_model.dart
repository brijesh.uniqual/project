
class User {
  String? id;
  String? name;
  String? email;
  String? password;

  User({required this.id, required this.name, required this.email, required this.password});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = {};
    data["id"] = id;
    data["name"] = name;
    data["password"] = password;
    data["email"] = email;

    return data;
  }

  User.fromJson(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    email = map['email'];
    password = map['password'];

  }



}