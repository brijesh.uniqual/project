
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class Gradient extends StatefulWidget {
  const Gradient({Key? key}) : super(key: key);

  @override
  State<Gradient> createState() => _GradientState();
}

class _GradientState extends State<Gradient> {

  var colors = [
    Colors.yellow.shade600,
    Colors.yellow.shade50,
  ];
  
  String buttonState = "Off";
  double opacity = 0;
  var icon = Icons.light_outlined;
  var bgColor = [
    Colors.white,
    Colors.white,
  ];

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    var screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: const Text("Gradient"),
      ),
      body: Stack(
        children: [
          AnimatedOpacity(
            opacity: opacity,
            duration: const Duration(seconds: 1),
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: bgColor,
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter
                  // begin: FractionalOffset(screenSize.width/2,0),
                  // end: FractionalOffset(screenSize.width/2,screenSize.height),
                ),
              ),
            ),
          ),
          AnimatedOpacity(
            opacity: opacity,
            duration: const Duration(seconds: 1),
            child: ClipPath(
              clipper: MyClipper(),
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: colors,
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter
                    // begin: FractionalOffset(screenSize.width/2,0),
                    // end: FractionalOffset(screenSize.width/2,screenSize.height),
                  ),
                ),
                // color: Colors.yellowAccent,
              ),
            ),
          ),
          Positioned(
            left: MediaQuery.of(context).size.width/2-50,
            top: -15,
            child: Icon(icon,size: 100,),
          ),
          if(buttonState == "On") ...{
            Positioned(
              left: MediaQuery.of(context).size.width / 2 - 25,
              top: MediaQuery.of(context).size.height / 2 - 20,
              child: SizedBox(
                width: 50,
                height: 40,
                child: Center(child: Text(buttonState, style: const TextStyle(fontSize: 26),)),
              ),
            ),
          },
          Positioned(
            bottom: 20,
            left: MediaQuery.of(context).size.width/2-120,
            child: SizedBox(
              width: 240,
              child: ElevatedButton(
                onPressed: () {
                  if(buttonState == "Off") {
                    opacity = 1.0;
                    buttonState = "On";
                    icon = Icons.light;
                    bgColor = [Colors.black12,Colors.white];
                  } else {
                    opacity = 0.0;
                    buttonState = "Off";
                    icon = Icons.light_outlined;
                    bgColor = [Colors.white,Colors.white];
                  }
                  setState(() {});
                },
                child: Text("Turn ${buttonState == "Off" ? "On" : "Off"} Lights",style: const TextStyle(fontSize: 26),),
              ),
            ),
          )
        ],
      )
    );
  }
}


class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.moveTo(size.width/2+25, 50);

    path.lineTo(size.width/2-23, 50);
    path.lineTo(0, size.height-200);
    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, size.height-200);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;

}



