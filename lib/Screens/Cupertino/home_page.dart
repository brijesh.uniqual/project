
import 'package:flutter/cupertino.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        previousPageTitle: "Previous Page",
        leading: Icon(CupertinoIcons.back),
        middle: Text("Cupertino App"),
      ),
      resizeToAvoidBottomInset: true,
      child: CupertinoButton(
        color: CupertinoColors.destructiveRed,
        onPressed: () {

        },
        child: const Center(
          child: Text("Cupertino"),
        ),
      ),
    );
  }
}
