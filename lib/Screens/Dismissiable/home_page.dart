
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> datas = List<String>.generate(50, (index) => "Item ${index+1}");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dismissible List"),
      ),
      body: ListView.builder(
          itemCount: datas.length,
          itemBuilder: (_,index) {
            return Dismissible(
              key: UniqueKey(),
              child: Card(
                color: Colors.grey.shade700,
                child: ListTile(
                  title: Center(child: Text(datas[index],style: const TextStyle(color: Colors.white,fontSize: 24),)),
                ),
              ),
              onDismissed: (d){
                String d = datas.removeAt(index);
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Item $d"),
                      action: SnackBarAction(
                          label: "Undo",
                          onPressed: () {
                            if(index>datas.length) {
                              datas.insert(datas.length, d);
                            } else {
                              datas.insert(index, d);
                            }
                            setState(() {});
                          }
                      ),
                    )
                );
              },
            );
          }
      ),
    );
  }
}

