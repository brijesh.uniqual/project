
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  late TapGestureRecognizer tapGestureRecognizer;
  late TapGestureRecognizer tapGestureRecognizer2;


  @override
  void initState() {
    super.initState();
    tapGestureRecognizer = TapGestureRecognizer()..onTap = handleTap;
    tapGestureRecognizer2 = TapGestureRecognizer()..onTap = handleTap2;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Rich Text"),
      ),
      body: Column(
        children: [
          Center(
            child: RichText(
              locale: const Locale('ja', 'JP'),
              textScaler: const TextScaler.linear(1.2),
              // maxLines: 2,
              textDirection: TextDirection.ltr,
              softWrap: true,
              text: TextSpan(
                style: const TextStyle(color: Colors.grey,fontSize: 16),
                children: [
                  TextSpan(recognizer: TapGestureRecognizer()..onTap = handleTap,spellOut: true,text: "Brijesh ",style: const TextStyle(color: Colors.green,fontWeight: FontWeight.bold,fontSize: 32)),
                  const TextSpan(text: "Bhut"),
                   TextSpan(semanticsLabel: "Label",recognizer: tapGestureRecognizer2,text: "Brijesh ",style: const TextStyle(color: Colors.blueGrey,fontWeight: FontWeight.bold,fontSize: 32)),
                  const TextSpan(text: "Bhut"),
                  const TextSpan(text: "Brijesh ",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold,fontSize: 32)),
                  const TextSpan(text: "Bhut"),
                  const TextSpan(text: "Brijesh ",style: TextStyle(color: Colors.orange,fontWeight: FontWeight.bold,fontSize: 32)),
                  const TextSpan(text: "Bhut"),
                  const TextSpan(text: "Brijesh ",style: TextStyle(color: Colors.indigoAccent,fontWeight: FontWeight.bold,fontSize: 32)),
                  const TextSpan(text: "Bhutб⟩, ⟨в⟩, ⟨г⟩, ⟨д⟩",style: TextStyle(locale: Locale('ru', 'RU'))),
                ],
              ),

            ),
          ),
          // const AutoSizeText(
          //   "Lorem tof Lorem Ipsum.",
          //   maxFontSize: 60,
          //   minFontSize: 3,
          //   maxLines: 2,
          // ),
        ],
      ),
    );
  }

  void handleTap() {
    print("brijesh green clicked");
  }
  void handleTap2() {
    print("brijesh grey clicked");
  }
}
