
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  bool row1 = false;
  bool row2 = false;
  bool row3 = false;
  bool all = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Data Table"),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          border: TableBorder.all(),
          onSelectAll: (bo) {
            setState(() {
              all = !all;
            });
          },
          // dividerThickness: 0.1,
          showBottomBorder: true,
          columns: const [
            DataColumn(label: Text("Name",)),
            DataColumn(label: Text("Surnamecccccccccc")),
            DataColumn(label: Text("Name")),
            DataColumn(label: Text("Surname")),
            DataColumn(label: Text("Name")),
            DataColumn(label: Text("Surname")),

          ],
          rows:  [
            DataRow(
              selected: all || row1,
                onSelectChanged: (b) {
                  setState(() {
                    row1 = !row1;
                  });
                },
              cells: const [
                DataCell(Text("Brijesh")),
                DataCell(Center(child: Text("Bhut"))),
                DataCell(Text("Brijesh")),
                DataCell(Text("Bhut")),
                DataCell(Text("Brijesh")),
                DataCell(Text("Bhut")),
              ],
            ), DataRow(
              selected: all || row2,
              onSelectChanged: (b) {
                setState(() {
                  row2 = !row2;
                });
              },
              cells: const [
                DataCell(Text("Bhut")),
                DataCell(Text("Brijesh")),
                DataCell(Text("Bhut")),
                DataCell(Text("Brijesh")),
                DataCell(Text("Bhut")),
                DataCell(Text("Brijesh")),
              ],
            ), DataRow(
              selected: all || row3,
              onSelectChanged: (b) {
                setState(() {
                  row3 = !row3;
                });
              },
              cells: const [
                DataCell(Text("Brijesh")),
                DataCell(Text("Bhut")),
                DataCell(Text("Brijesh")),
                DataCell(Text("Bhut")),
                DataCell(Text("Brijesh")),
                DataCell(Text("Bhut")),
              ],
            ),
          ],

        ),
      ),
    );
  }
}
