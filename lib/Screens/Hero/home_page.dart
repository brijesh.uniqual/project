
import 'dart:math';

import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  var key = UniqueKey();

  @override
  void initState() {
    super.initState();

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Hero"),
      ),
      body: Center(
        child: InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => ImageView(imagekey: key)));
          },
          child: Hero(
            // placeholderBuilder: ,
            // flightShuttleBuilder: (BuildContext flightContext,
            //     Animation<double> animation,
            //     HeroFlightDirection flightDirection,
            //     BuildContext fromHeroContext,
            //     BuildContext toHeroContext,) {
            //   final Widget toHero = toHeroContext.widget;
            //   return FlipcardTransition(
            //     flipAnim: animation,
            //     child: toHero,
            //   );
            // },
            transitionOnUserGestures: false,
            // createRectTween: _createRectTween,
            // key: key,
            tag: key,
            child: Container(
              height: 100,
              width: 100,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage("https://st2.depositphotos.com/2001755/5408/i/450/depositphotos_54081723-stock-photo-beautiful-nature-landscape.jpg"),
                ),
              ),
              // child: Hero(
              //     tag: key,
              //     child: Image.network("https://st2.depositphotos.com/2001755/5408/i/450/depositphotos_54081723-stock-photo-beautiful-nature-landscape.jpg"),
              // ),
            ),
          ),
        ),
      ),
    );
  }

  // static RectTween _createRectTween(Rect? begin, Rect? end) {
  //   return MaterialRectCenterArcTween(begin: begin, end: end);
  // }
}

class ImageView extends StatelessWidget {
  const ImageView({Key? key,required this.imagekey}) : super(key: key);

  final imagekey;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Hero(
          tag: imagekey,
          child: Image.network("https://st2.depositphotos.com/2001755/5408/i/450/depositphotos_54081723-stock-photo-beautiful-nature-landscape.jpg"),
        ),
      ),
    );
  }
}


class FlipcardTransition extends AnimatedWidget {
  final Animation<double> flipAnim;
  final Widget child;

  FlipcardTransition({required this.flipAnim, required this.child})
      : assert(flipAnim != null),
        assert(child != null),
        super(listenable: flipAnim);

  @override
  Widget build(BuildContext context) {
    return Transform(
      transform: Matrix4.identity()
        ..rotateX(-pi + flipAnim.value),
      alignment: FractionalOffset.center,
      child: child,
    );
  }
}