
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.camerasList}) : super(key: key);

  final List<CameraDescription> camerasList;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


  late CameraController cameraController;
  XFile? image;



  @override
  void initState() {
    super.initState();

    // getCameralist();

    cameraController = CameraController(widget.camerasList[0], ResolutionPreset.low);


    cameraController.initialize()
        .then(
            (value) {
          if(!mounted) {
            return ;
          }
          setState(() {});
        }
    );

  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: const Text("Camera"),
      ),
      body: getBody(),
      floatingActionButton: FloatingActionButton(
        onPressed: () async{

          cameraController.initialize()
              .then(
                  (value) {
                if(!mounted) {
                  return ;
                }
                setState(() {});
              }
          );

          try {
            image = await cameraController.takePicture();
          } catch(e) {
            print(e);
          }

          final path = join(
            // Store the picture in the temp directory.
            // Find the temp directory using the `path_provider` plugin.
            (await getTemporaryDirectory()).path,
            '${DateTime.now()}.png',
          );
          image!.saveTo(path);

          if(!mounted) {
            return;
          }
          
          Navigator.push(context, MaterialPageRoute(builder: (context) => DisplayImage(imagePath: path,)));
          // setState(() {});

        },

      ),
    );
  }
  //
  // getCameralist() async{
  //
  //   camerasList = await availableCameras();
  //
  // }

  getBody() {
    if(!cameraController.value.isInitialized) {
      return const Placeholder();
    }
    return AspectRatio(
      aspectRatio: cameraController.value.aspectRatio,
      child: CameraPreview(
        cameraController,
        child: const Text("data"),
      ),
    );
  }

  _HomePageState();
}

class DisplayImage extends StatelessWidget {
  const DisplayImage({Key? key, required this.imagePath}) : super(key: key);

  final String imagePath;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Image.file(File(imagePath)),
    );
  }
}

