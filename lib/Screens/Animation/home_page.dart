
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin{
  double age = 0;
  late DateTime? selectedDate;
  late Animation animation;
  late AnimationController animationController;

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(vsync: this,duration: const Duration(milliseconds: 5500));

    animation = animationController;

  }

  @override
  void dispose() {
    animationController.removeListener(() { });
    animation.removeListener(() { });
    animationController.dispose();

    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Animation"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () async{

                  animationController.reset();
                  animation.removeListener(() { });

                  selectedDate = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(1900), lastDate: DateTime.now());

                  age = DateTime.now().year.toDouble() - selectedDate!.year.toDouble();

                  animation = Tween<double>(begin: 0.0, end: age).animate(CurvedAnimation(parent: animationController, curve: Curves.fastLinearToSlowEaseIn));

                  animation.addListener(() {
                    setState(() {});
                  });

                  animationController.forward();


                  setState(() {});
                },
                child: const Text("Select Your Birth date")
            ),
            const SizedBox(height: 30,),
            Text("Your age is ${double.parse(animation.value.toString()).toStringAsFixed(0)}",style: const TextStyle(fontSize: 30),),
          ],
        ),
      ),
    );
  }
}

