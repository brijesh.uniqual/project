
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late GoogleMapController mapController;
  late LatLng currentPostion;
  bool isLoading = true;

  // final LatLng _center = const LatLng(45.521563, -122.677433);
  // final LatLng _center = const LatLng(21.1702, 72.8311);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  late Marker markers;


  @override
  void initState() {
    super.initState();

    initilizeData();

  }

  initilizeData() async{
    
    bool isPermission = await _checkPermission();
    if(isPermission) {
      await _getUserLocation();

      markers = Marker(
        markerId: const MarkerId('Surat'),
        position: currentPostion,
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Permission denied')));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        useMaterial3: true,
        colorSchemeSeed: Colors.green[700],
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Google Maps Sample App'),
          elevation: 2,
        ),
        body: isLoading
            ? const Center(child: CircularProgressIndicator(),)
            : GoogleMap(
                // tileOverlays: ,
                onTap: (l) {
                  markers = Marker(
                    markerId: const MarkerId('CustomMarker'),
                    position: l,
                  );
                  setState(() {});
                },
                markers: {
                  markers
                },
                myLocationButtonEnabled: true,
                myLocationEnabled: true,
                onMapCreated: _onMapCreated,
                initialCameraPosition: CameraPosition(
                  target: currentPostion,
                  zoom: 11.0,
                ),
              ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            _getUserLocation();
          },
          icon: const Icon(Icons.home),
          label: const Text("Go to Home"),

        ),
      ),
    );
  }


   _getUserLocation() async {
    setState(() {
      isLoading = true;
    });
    var position = await GeolocatorPlatform.instance.getCurrentPosition();

    setState(() {
      isLoading = false;
      currentPostion = LatLng(position.latitude, position.longitude);
      markers = Marker(
        markerId: const MarkerId('CustomMarker'),
        position: currentPostion,
      );
    });
  }

  Future<bool> _checkPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return true;
  }



}