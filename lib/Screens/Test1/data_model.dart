
class Category {
  String? title;
  List<SubCategory>? subcategories;

  Category({required this.title, required this.subcategories});

  Category.fromJson(Map<String, dynamic> json) {
        title = json['title'];
        if (json['subcategories'] != null) {
          subcategories = <SubCategory>[];
          json['subcategories'].forEach((v) {
            subcategories!.add(SubCategory.fromJson(v));
          });
        }
  }
}

class SubCategory {
  String? title;
  List<String>? images;

  SubCategory({required this.title, required this.images});

  SubCategory.fromJson(Map<String, dynamic> json) {
      title = json['title'];
      images = json['image'].cast<String>();

  }

}