
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/Screens/Test1/subcategory.dart';
import 'package:get/get.dart';

import 'data_controller.dart';
import 'data_model.dart';

class Test1 extends StatefulWidget {
  const Test1({Key? key}) : super(key: key);

  @override
  State<Test1> createState() => _Test1State();
}

class _Test1State extends State<Test1> {

  List<Category> imageData = [];

  @override
  void initState() {
    super.initState();

    var datas = json.decode(jsonData);

    imageData = List<Category>.from(datas.map((e) => Category.fromJson(e)));

    print(imageData);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Test"),
      ),
      body: ListView.builder(
        itemCount: imageData.length,
        itemBuilder: (context,i) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 10),
                  child: Text("${imageData[i].title!.capitalizeFirst!} $i",style: const TextStyle(fontSize: 26),),
                ),
                SizedBox(
                  height: 300,
                  // width: MediaQuery.of(context).size.width,
                  child: SubCategoryList(subcategory: imageData[i].subcategories!),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
