
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'data_model.dart';

class SubCategoryList extends StatefulWidget {
  List<SubCategory> subcategory;
  SubCategoryList({Key? key, required this.subcategory}) : super(key: key);

  @override
  State<SubCategoryList> createState() => _SubCategoryState();
}

class _SubCategoryState extends State<SubCategoryList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      itemCount: widget.subcategory.length,
      itemBuilder: (context,j) {
        return SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  // height: 220,
                  width: MediaQuery.of(context).size.width/2,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                    itemCount: widget.subcategory[j].images!.length,
                    itemBuilder: (context,k) {
                      return ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: Container(
                          height: 500,
                          width: 500,
                          decoration: BoxDecoration(
                            image: DecorationImage(fit:BoxFit.cover,image: NetworkImage(widget.subcategory[j].images![k])),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Text("${widget.subcategory[j].title!.capitalizeFirst!} $j",style: const TextStyle(fontSize: 20),),
              ],
            ),
          ),
        );
      },
    );
  }
}
