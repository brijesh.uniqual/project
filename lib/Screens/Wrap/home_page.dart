
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {


  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Wrap"),
      ),
      body: Container(
        color: Colors.amber,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Wrap(
          direction: Axis.horizontal,
          // alignment: WrapAlignment.center,
          // alignment: WrapAlignment.center,
          // runAlignment: WrapAlignment.center,
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: 20,
          runSpacing: 50,
          children: [
            const Text(style: TextStyle(fontSize: 30), "BrijeshBhut"),
            const Text(style: TextStyle(fontSize: 30), "BrijeshBhut"),
            const Text(style: TextStyle(fontSize: 30), "BrijeshBhut"),
            const Text(style: TextStyle(fontSize: 30), "BrijeshBhut"),
            const Text(style: TextStyle(fontSize: 30), "BrijeshBhut"),
            const Text(style: TextStyle(fontSize: 30), "BrijeshBhut"),
            Container(
              width: 50,
              height: 200,
              color: Colors.greenAccent,
              child: Center(child: const Text("Contain")),
            ),

          ],
        ),
      ),
    );
  }
}
