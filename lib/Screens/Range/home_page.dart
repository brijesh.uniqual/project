
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var values = const RangeValues(18, 100);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Range Slider"),
      ),
      body: Center(
        child: RangeSlider(
          values: values,
          min: 18,
          max: 100,
          divisions: 20,
          overlayColor: MaterialStateProperty.all(Colors.green),
          labels: RangeLabels(values.start.toString(), values.end.toString()),
          onChangeStart: (v) => print("Start"),
          onChanged: (newValues) {
            print("Changed");
            values = newValues;

            setState(() {});
          },
          onChangeEnd: (v) => print("End"),
        ),
      ),
    );
  }
}
