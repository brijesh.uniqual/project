import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'square.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final FocusNode _focusNode = FocusNode();

  List<Box> boxes = [];
  int emptyBoxIndex = 9;
  bool updisable = false;
  bool downdisable = true;
  bool rightdisable = true;
  bool leftdisable = false;
  bool youWon = false;
  bool isGameFinished = false;

  List<Box> doInit() {
    emptyBoxIndex = 9;
    updisable = false;
    downdisable = true;
    rightdisable = true;
    leftdisable = false;
    youWon = false;
    var boxes = <Box>[
      Box(2),
      Box(5),
      Box(1),
      Box(3),
      Box(8),
      Box(6),
      Box(7),
      Box(4),
      Box(0),
    ];

    boxes.shuffle();

    for(Box b in boxes) {
      if(b.id == 0) {
        emptyBoxIndex = boxes.indexOf(b)+1;
      }
    }
    // emptyBoxIndex = boxes.indexOf(Box(0))+1;
    updateButtonState(emptyBoxIndex);
    return boxes;
  }

  @override
  void initState() {
    super.initState();
    boxes = doInit();
  }

  updateBoxesList(int oldIndex, int newIndex) {
    Box oldBox = boxes[oldIndex-1];
    boxes[oldIndex-1] = boxes.elementAt(newIndex-1);
    boxes[newIndex-1] = oldBox;
  }

  updateButtonState(int buttonId) {
    switch(buttonId) {
      case 1:
        updisable = true;
        downdisable = false;
        rightdisable = false;
        leftdisable = true;
        break;

      case 2:
        updisable = true;
        downdisable = false;
        rightdisable = false;
        leftdisable = false;
        break;

      case 3:
        updisable = true;
        downdisable = false;
        rightdisable = true;
        leftdisable = false;
        break;

      case 4:
        updisable = false;
        downdisable = false;
        rightdisable = false;
        leftdisable = true;
        break;

      case 5:
        updisable = false;
        downdisable = false;
        rightdisable = false;
        leftdisable = false;
        break;

      case 6:
        updisable = false;
        downdisable = false;
        rightdisable = true;
        leftdisable = false;
        break;

      case 7:
        updisable = false;
        downdisable = true;
        rightdisable = false;
        leftdisable = true;
        break;

      case 8:
        updisable = false;
        downdisable = true;
        rightdisable = false;
        leftdisable = false;
        break;

      case 9:
        updisable = false;
        downdisable = true;
        rightdisable = true;
        leftdisable = false;
        break;
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Game"),
      ),
      body: RawKeyboardListener(
        autofocus: true,
        focusNode: _focusNode,
        onKey: _handleKeyEvent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: GridView.builder(
                padding: const EdgeInsets.all(10.0),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3, childAspectRatio: 1.0, crossAxisSpacing: 9.0, mainAxisSpacing: 9.0),
                itemCount: boxes.length,
                itemBuilder: (context, i) => SizedBox(
                  width: 100.0,
                  height: 100.0,
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    color: boxes[i].id ==0 ? Colors.red : Colors.tealAccent,
                    child: Center(
                      child: Text(
                        boxes[i].id == 0 ? "" : boxes[i].id.toString(),
                        style: const TextStyle(color: Colors.black, fontSize: 20.0),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  decoration: const BoxDecoration(shape: BoxShape.circle),
                  child: TextButton(
                      onPressed: () {
                        int oldIndex = emptyBoxIndex;
                        leftdisable
                            ? null
                            : emptyBoxIndex -= 1;

                        updateBoxesList(oldIndex, emptyBoxIndex);

                        updateButtonState(emptyBoxIndex);
                        print(emptyBoxIndex);
                        if(emptyBoxIndex == 5) {
                          checkForGameOver();
                        }
                        setState(() {});
                      },
                      child: const Icon(
                        Icons.arrow_circle_left_outlined,
                        size: 45,
                        color: Colors.black,
                      )),
                ),
                Container(
                  decoration: const BoxDecoration(shape: BoxShape.circle),
                  child: TextButton(

                      onPressed: () {
                        int oldIndex = emptyBoxIndex;
                        rightdisable
                            ? null
                            : emptyBoxIndex += 1;

                        updateBoxesList(oldIndex, emptyBoxIndex);

                        updateButtonState(emptyBoxIndex);
                        print(emptyBoxIndex);
                        if(emptyBoxIndex == 5) {
                          checkForGameOver();
                        }
                        setState(() {});
                      },
                      child: const Icon(
                        Icons.arrow_circle_right_outlined,
                        size: 45,
                        color: Colors.black,
                      )),
                ),
                Container(
                  decoration: const BoxDecoration(shape: BoxShape.circle),
                  child: TextButton(
                      onPressed: () {
                        int oldIndex = emptyBoxIndex;
                        updisable
                            ? null
                            : emptyBoxIndex -= 3;

                        updateBoxesList(oldIndex, emptyBoxIndex);

                        updateButtonState(emptyBoxIndex);
                        print(emptyBoxIndex);
                        if(emptyBoxIndex == 5) {
                          checkForGameOver();
                        }
                        setState(() {});
                      },
                      child: const Icon(
                        Icons.arrow_circle_up,
                        size: 45,
                        color: Colors.black,
                      )),
                ),
                Container(
                  decoration: const BoxDecoration(shape: BoxShape.circle),
                  child: TextButton(
                      onPressed: () {
                        int oldIndex = emptyBoxIndex;
                        downdisable
                            ? null
                            : emptyBoxIndex += 3;

                        updateBoxesList(oldIndex, emptyBoxIndex);

                        updateButtonState(emptyBoxIndex);
                        print(emptyBoxIndex);
                        if(emptyBoxIndex == 5) {
                          checkForGameOver();
                        }
                        setState(() {});
                      },
                      child: const Icon(
                        Icons.arrow_circle_down,
                        size: 45,
                        color: Colors.black,
                      )),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    onPressed: () {
                      isGameFinished = false;
                      resetGame();
                    },
                    child: const Text("Reset Game")
                ),
              ],
            ),
          ],
        ),

      ),
    );
  }

  checkForGameOver() {
    if(checkWinningList()) {
      youWon = true;
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
            title: const Text("You Won"),
            content: const Text("You arrange all Boxes at right place"),
            actions: <Widget>[
              ElevatedButton(
                onPressed: () {
                  isGameFinished = true;
                  resetGame();
                },
                child: const Text("Reset"),
              )
            ],
          )
      );
    } else {
      youWon = false;
    }
  }

  resetGame() {
    if(Navigator.canPop(context) && isGameFinished) {
      Navigator.pop(context);
    }
    boxes = doInit();
    setState(() {});
  }

  void _handleKeyEvent(RawKeyEvent event) {

    if (event.logicalKey == LogicalKeyboardKey.arrowUp && event.isKeyPressed(LogicalKeyboardKey.arrowUp)) {
      int oldIndex = emptyBoxIndex;
      updisable
          ? null
          : emptyBoxIndex -= 3;

      updateBoxesList(oldIndex, emptyBoxIndex);

      updateButtonState(emptyBoxIndex);
      print(emptyBoxIndex);
      if(emptyBoxIndex == 5) {
        checkForGameOver();
      }
      setState(() {});
      print("key");
    } else if (event.logicalKey == LogicalKeyboardKey.arrowDown && event.isKeyPressed(LogicalKeyboardKey.arrowDown)) {
      int oldIndex = emptyBoxIndex;
      downdisable
          ? null
          : emptyBoxIndex += 3;

      updateBoxesList(oldIndex, emptyBoxIndex);

      updateButtonState(emptyBoxIndex);
      print(emptyBoxIndex);
      if(emptyBoxIndex == 5) {
        checkForGameOver();
      }
      setState(() {});
      print("key");
    } else if (event.logicalKey == LogicalKeyboardKey.arrowLeft && event.isKeyPressed(LogicalKeyboardKey.arrowLeft)) {
      int oldIndex = emptyBoxIndex;
      leftdisable
          ? null
          : emptyBoxIndex -= 1;

      updateBoxesList(oldIndex, emptyBoxIndex);

      updateButtonState(emptyBoxIndex);
      print(emptyBoxIndex);
      if(emptyBoxIndex == 5) {
        checkForGameOver();
      }
      setState(() {});
      print("key");
    }else if (event.logicalKey == LogicalKeyboardKey.arrowRight && event.isKeyPressed(LogicalKeyboardKey.arrowRight)) {
      int oldIndex = emptyBoxIndex;
      rightdisable
          ? null
          : emptyBoxIndex += 1;

      updateBoxesList(oldIndex, emptyBoxIndex);

      updateButtonState(emptyBoxIndex);
      print(emptyBoxIndex);
      if(emptyBoxIndex == 5) {
        checkForGameOver();
      }
      setState(() {});
      print("key");
    }
  }

  bool checkWinningList() {

    if(boxes[4].id == 0) {
      int temp = boxes[0].id;

      if((boxes[0].id == ((temp > 8) ? temp-8: temp) &&
          boxes[1].id == ((temp + 1 > 8) ? temp+1-8: temp+1) &&
          boxes[2].id == ((temp + 2 > 8) ? temp+2-8: temp+2) &&
          boxes[5].id == ((temp + 3 > 8) ? temp+3-8: temp+3) &&
          boxes[8].id == ((temp + 4 > 8) ? temp+4-8: temp+4) &&
          boxes[7].id == ((temp + 5 > 8) ? temp+5-8: temp+5) &&
          boxes[6].id == ((temp + 6 > 8) ? temp+6-8: temp+6) &&
          boxes[3].id == ((temp + 7 > 8) ? temp+7-8: temp+7)) ||
          (boxes[0].id == ((temp > 8) ? temp-8: temp) &&
          boxes[3].id == ((temp + 1 > 8) ? temp+1-8: temp+1) &&
          boxes[6].id == ((temp + 2 > 8) ? temp+2-8: temp+2) &&
          boxes[7].id == ((temp + 3 > 8) ? temp+3-8: temp+3) &&
          boxes[8].id == ((temp + 4 > 8) ? temp+4-8: temp+4) &&
          boxes[5].id == ((temp + 5 > 8) ? temp+5-8: temp+5) &&
          boxes[2].id == ((temp + 6 > 8) ? temp+6-8: temp+6) &&
          boxes[1].id == ((temp + 7 > 8) ? temp+7-8: temp+7))) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
