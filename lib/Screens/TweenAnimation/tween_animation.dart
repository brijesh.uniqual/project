
import 'package:flutter/material.dart';

class AnimationTween extends StatefulWidget {
  const AnimationTween({Key? key}) : super(key: key);

  @override
  State<AnimationTween> createState() => _AnimationTweenState();
}

class _AnimationTweenState extends State<AnimationTween> with SingleTickerProviderStateMixin {

  late Animation animation;
  late AnimationController animationController;

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(vsync: this, duration: const Duration(seconds: 4));
    animation = Tween(begin: 0.0,end: 200.0).animate(animationController);

    animation.addListener(() {
      setState(() {});
    });

    animationController.forward().then((value) => animationController.repeat());

  }

  @override
  void dispose() {
    animationController.dispose();

    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tween Animation"),
      ),
      body: Center(
        child: Container(
          width: animation.value,
          height: animation.value,
          color: Colors.indigoAccent,
        ),
      ),
    );
  }
}
