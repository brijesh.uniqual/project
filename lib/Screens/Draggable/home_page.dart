
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  int result = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Draggable"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Draggable(
              feedback: Container(
                height: 100,
                width: 100,
                color: Colors.redAccent,
              ),
              childWhenDragging: Container(
                height: 100,
                width: 100,
                color: Colors.green,
              ),
              data: 1,
              child: Container(
                height: 100,
                width: 100,
                color: Colors.blue,
              ),
            ),
            DragTarget(
              builder: (BuildContext context,List<dynamic> accept,List<dynamic> reject) {
                return Container(
                  height: 100,
                  width: 100,
                  color: Colors.grey,
                  child: Center(child: Text("$result",style: const TextStyle(fontSize: 26),)),
                );
              },
              hitTestBehavior: HitTestBehavior.opaque,
              onAcceptWithDetails: (DragTargetDetails details) {
                print("onAcceptWithDetails");
                setState(() {
                  result += int.parse(details.data!.toString());
                });
              },
              onLeave: (D) {
                print("onLeave");
              },
              onMove: (D) {
                print("onMove");
              },
              onWillAcceptWithDetails: (D) {
                print("onWillAcceptWithDetails");
                return true;
              },
            ),
          ],
        ),
      ),
    );
  }
}
