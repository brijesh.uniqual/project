
import 'package:get/get.dart';

class MainHomeController extends GetxController  {

  List<String> data = [
    "Calculator",
    "Chat App",
    "Login Page",
    "Network Call",
    "Clipping",
    // "database_sqlite",
    "Dismissible List",
    "Websocket",
    "Bottom Sheet",
    // "Firebase",
    "Game",
    "Animation",
    "Facebook",
    "Orientation/Sliver App Bar",
    "Sqlite Practical 1",
    "UI Practical 2",
    "Paint",
    "Camera",
    "Connectivity",
    "Refresh",
    "Shopping Cart Redux",
    "Hero",
    "Google Map",
    "List Types/Back Drop",
    "GetX Example",
    "Pop Scope",
    "Animation 2",
    "Draggable",
    "Cupertino",
    "Data Table",
    "WebView Widget",
    "Wrap",
    "Stack",
    "RichText",
    "Icon",
    "ConstrainBox",
    "Range Slider",
    "Animated Container",
    "ListWheel",
    "ClipRect/ClipRRect",
    "Gradient",
    "Tween Animation",
    "Shared Pref",
    "Test 1",
  ];

  List<String> filter = [];

  getData(String filterString) {

    filter.clear();

    if(filterString != "") {
      for (String s in data) {
        if (s.toLowerCase().contains(filterString.toLowerCase())) {
          filter.add(s);
        }
      }
    } else {
      filter.addAll(data);
    }

    update();
  }
}