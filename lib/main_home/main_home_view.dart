import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/main_home/main_home_controller.dart';
import 'package:get/get.dart';

class MainHomeView extends StatelessWidget {
  MainHomeView({super.key});

  TextEditingController serachController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter App ${Random().nextInt(100)}"),
      ),
      body: GetBuilder<MainHomeController>(
          init: MainHomeController(),
          dispose: (_) => Get.delete<MainHomeController>(),
          builder: (_) {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextField(
                    // inputFormatters: [
                    //   FilteringTextInputFormatter.digitsOnly
                    // ],
                    // maxLength: 2,
                    // // keyboardType: TextInputType.number,
                    controller: serachController,
                    onChanged: (value) {
                      _.getData(value);
                    },
                    onTapOutside: (v) {
                      FocusManager.instance.primaryFocus?.unfocus();
                    },
                    decoration: InputDecoration(
                      // counterText: "",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      hintText: "Search Here...",
                    ),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: (_.filter.isEmpty && serachController.value.text == "") ? _.data.length : _.filter.length,
                    itemBuilder: (context, i) {
                      return Card(
                        elevation: 0,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              gradient: const LinearGradient(
                                begin: Alignment.bottomLeft,
                                end: Alignment.topRight,
                                colors: [
                                  Colors.blue,
                                  Colors.deepPurple,
                                  Colors.red,
                                ],
                              )
                          ),
                          height: 70,
                          child: InkWell(
                            onTap: () {
                              Get.toNamed(
                                  "/${(_.filter.isEmpty && serachController.value.text == "") ? _.data[i].replaceAll(" ", "-") : _.filter[i].replaceAll(" ", "-")}");
                            },
                            child: Center(
                                child: Text(
                              (_.filter.isEmpty && serachController.value.text == "") ? _.data[i] : _.filter[i],
                              style: const TextStyle(fontSize: 26, color: Colors.white),
                            )),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          }
      ),
    );
  }
}
