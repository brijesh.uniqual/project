import 'package:camera/camera.dart';
import 'package:flutter_app/main_home/main_home_view.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:get/get.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';

import 'package:flutter/material.dart';

import 'package:flutter_app/Screens/SQLiteP1/DBHelper/db_helper.dart';
import 'package:flutter_app/Screens/ShoppingCartRedux/model/cart_item.dart';
import 'package:flutter_app/Screens/ShoppingCartRedux/redux/reducer.dart';

import 'package:flutter_app/Screens/Calculator/calculator.dart';
import 'package:flutter_app/Screens/ChatApp/home_screen.dart' as ChatApp;
import 'package:flutter_app/Screens/LoginPage/home_page.dart' as LoginPage;
import 'package:flutter_app/Screens/NetworkCall/home_page/home_page_view.dart' as NetworkCall;
import 'package:flutter_app/Screens/Clipping/my_home_page.dart' as Clipping;
// import 'package:flutter_app/Screens/DataBaseSQLite/Screens/home_screen.dart' as DatabaseSQL;
import 'package:flutter_app/Screens/Dismissiable/home_page.dart' as Dismissiable;
import 'package:flutter_app/Screens/WebSocket/my_home_page.dart' as WebSocket;
import 'package:flutter_app/Screens/BottomSheet/my_home_page.dart' as BottomSheet;
import 'package:flutter_app/Screens/Game/home_page.dart' as Game;
import 'package:flutter_app/Screens/Animation/home_page.dart' as Animation;
import 'package:flutter_app/Screens/FaceBook/facebook_home.dart';
import 'package:flutter_app/Screens/Orientation/home_page.dart' as Orientation;
import 'package:flutter_app/Screens/SQLiteP1/login_screen.dart';
import 'package:flutter_app/Screens/UI_P2/home_page.dart' as UI_P2;
import 'package:flutter_app/Screens/Paint/home_page.dart' as Paint;
import 'package:flutter_app/Screens/Camera/home_page.dart' as CameraAccess;
import 'package:flutter_app/Screens/Connectivity/home_page.dart' as Connetivity;
import 'package:flutter_app/Screens/Refresh/home_page.dart' as RefreshList;
import 'package:flutter_app/Screens/Hero/home_page.dart' as HeroWidget;
import 'package:flutter_app/Screens/ListType/home_page.dart' as ListType;
import 'package:flutter_app/Screens/GoogleMap/my_app.dart' as GoogleMap;
import 'package:flutter_app/Screens/ShoppingCartRedux/shopping_cart.dart' as ReduxCart;
import 'package:flutter_app/Screens/GetX/home_page.dart' as GetEx;
import 'package:flutter_app/Screens/PopScope/home_page.dart' as Pop;
import 'package:flutter_app/Screens/Animation2/home_page.dart' as Animation2;
import 'package:flutter_app/Screens/Draggable/home_page.dart' as Draggable;
import 'package:flutter_app/Screens/Cupertino/home_page.dart' as Cupertino;
import 'package:flutter_app/Screens/DataTable/home_page.dart' as DataTable;
import 'package:flutter_app/Screens/WebView/home_page.dart' as Web;
import 'package:flutter_app/Screens/Wrap/home_page.dart' as Wrap;
import 'package:flutter_app/Screens/Stack/home_page.dart' as Stack;
import 'package:flutter_app/Screens/RichText/home_page.dart' as Rich;
import 'package:flutter_app/Screens/icon_ex/home_page.dart' as IconEx;
import 'package:flutter_app/Screens/Constrain/home_page.dart' as Constrain;
import 'package:flutter_app/Screens/Range/home_page.dart' as Range;
import 'package:flutter_app/Screens/AnimatedContainer/animatedContainer.dart';
import 'package:flutter_app/Screens/ListWheel/list_wheel.dart';
import 'package:flutter_app/Screens/ClipRect/clipRect.dart';
import 'package:flutter_app/Screens/Gradient/gradient.dart' as GradientEx;
import 'package:flutter_app/Screens/TweenAnimation/tween_animation.dart';
import 'package:flutter_app/Screens/SharedPref/splash_screen.dart';
import 'package:flutter_app/Screens/Test1/data_view.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  List<CameraDescription> camerasList;
  camerasList = await availableCameras();
  print(camerasList);

  final db = DatabaseHelper();
  db.createDBUser();
  final store = DevToolsStore<List<CartItem>>(cartItemReducer,initialState: []);

  runApp(MyApp(cameraList: camerasList, store: store));
}

class MyApp extends StatelessWidget {
  final List<CameraDescription> cameraList;
  final DevToolsStore<List<CartItem>> store;
  const MyApp({super.key, required this.cameraList, required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: GetMaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Colors.blue,
          appBarTheme: const AppBarTheme(
            color: Colors.blue
          )
        ),
        home: MainHomeView(),
        initialRoute: '/',
        getPages: [
          // GetPage(name: '/' , page: () => const MyHomePage()),
          GetPage(name: '/Calculator' , page: () => const Calculator()),
          GetPage(name: '/Chat-App' , page: () => const ChatApp.HomeScreen()),
          GetPage(name: '/Login-Page' , page: () =>  const LoginPage.MyHomePage()),
          GetPage(name: '/Network-Call' , page: () =>  const NetworkCall.HomePageView()),
          GetPage(name: '/Clipping' , page: () => const Clipping.MyHomePage()),
          // GetPage(name: '/database_sqlite' , page: () => const DatabaseSQL.HomeScreen()),
          GetPage(name: '/Dismissible-List' , page: () => const Dismissiable.MyHomePage()),
          GetPage(name: '/Websocket' , page: () =>  WebSocket.MyHomePage()),
          GetPage(name: '/Bottom-Sheet' , page: () => BottomSheet.MyHomePage()),
          // GetPage(name: '/firebase' , page: () =>  ),
          GetPage(name: '/Game' , page: () => const Game.MyHomePage()),
          GetPage(name: '/Animation' , page: () => Animation.MyHomePage()),
          GetPage(name: '/Facebook' , page: () =>  const FacebookHome()),
          GetPage(name: '/Orientation/Sliver-App-Bar' , page: () =>  const Orientation.MyHomePage()),
          GetPage(name: '/Sqlite-Practical-1' , page: () => const LoginScreen()),
          GetPage(name: '/UI-Practical-2' , page: () => const UI_P2.HomePage()),
          GetPage(name: '/Paint' , page: () => const Paint.HomePage()),
          GetPage(name: '/Camera' , page: () =>  CameraAccess.HomePage(camerasList: cameraList,)),
          GetPage(name: '/Connectivity' , page: () => const Connetivity.HomePage()),
          GetPage(name: '/Refresh' , page: () => const RefreshList.HomePage()),
          GetPage(name: '/Shopping-Cart-Redux' , page: () => ReduxCart.ShoppingCart(store: store,)),
          GetPage(name: '/Hero' , page: () =>  const HeroWidget.HomePage()),
          GetPage(name: '/Google-Map' , page: () =>  const GoogleMap.MyApp()),
          GetPage(name: '/List-Types/Back-Drop' , page: () =>  const ListType.HomePage()),
          GetPage(name: '/GetX-Example' , page: () => const GetEx.HomePage()),
          GetPage(name: '/Pop-Scope', page: () => const Pop.HomePage()),
          GetPage(name: '/Animation-2', page: () => const Animation2.HomePage()),
          GetPage(name: '/Draggable', page: () => const Draggable.HomePage()),
          GetPage(name: '/Cupertino', page: () => const Cupertino.HomePage()),
          GetPage(name: '/Data-Table', page: () => const DataTable.HomePage()),
          GetPage(name: '/WebView-Widget', page: () => const Web.WebViewExample()),
          GetPage(name: '/Wrap', page: () => const Wrap.HomePage()),
          GetPage(name: '/Stack', page: () => const Stack.HomePage()),
          GetPage(name: '/RichText', page: () => const Rich.HomePage()),
          GetPage(name: '/Icon', page: () => const IconEx.HomePage()),
          GetPage(name: '/ConstrainBox', page: () => const Constrain.HomePage()),
          GetPage(name: '/Range-Slider', page: () => const Range.HomePage()),
          GetPage(name: '/Animated-Container', page: () => const AnimationContainer()),
          GetPage(name: '/ListWheel', page: () => ListWheel()),
          GetPage(name: '/ClipRect/ClipRRect', page: () => const ClipRectWidget()),
          GetPage(name: '/Gradient', page: () => const GradientEx.Gradient()),
          GetPage(name: '/Tween-Animation', page: () => const AnimationTween()),
          GetPage(name: '/Shared-Pref', page: () => const SplashScreen()),
          GetPage(name: '/Test-1', page: () => const Test1()),

        ],
        // routes: {
        //   // // '/' : (context) => const MyHomePage(),
        //   // '/Calculator' : (context) => const Calculator(),
        //   // '/Chat App' : (context) => const ChatApp.HomeScreen(),
        //   // '/Login Page' : (context) =>  const LoginPage.MyHomePage(),
        //   // '/Network Call' : (context) =>  const NetworkCall.HomePage(),
        //   // '/Clipping' : (context) => const Clipping.MyHomePage(),
        //   // // '/database_sqlite' : (context) => const DatabaseSQL.HomeScreen(),
        //   // '/Dismissible List' : (context) => const Dismissiable.MyHomePage(),
        //   // '/Websocket' : (context) =>  WebSocket.MyHomePage(),
        //   // '/Bottom Sheet' : (context) => BottomSheet.MyHomePage(),
        //   // // '/firebase' : (context) =>  ,
        //   // '/Game' : (context) => const Game.MyHomePage(),
        //   // '/Animation' : (context) => Animation.MyHomePage(),
        //   // '/Facebook' : (context) =>  const FacebookHome(),
        //   // '/Orientation/Sliver App Bar' : (context) =>  const Orientation.MyHomePage(),
        //   // '/Sqlite Practical 1' : (context) => const LoginScreen(),
        //   // '/UI Practical 2' : (context) => const UI_P2.HomePage(),
        //   // '/Paint' : (context) => const Paint.HomePage(),
        //   // '/Camera' : (context) =>  CameraAccess.HomePage(camerasList: cameraList,),
        //   // '/Connectivity' : (context) => const Connetivity.HomePage(),
        //   // '/Refresh' : (context) => const RefreshList.HomePage(),
        //   // '/Shopping Cart Redux' : (context) => ReduxCart.ShoppingCart(store: store,),
        //   // '/Hero' : (context) =>  const HeroWidget.HomePage(),
        //   // '/Google Map' : (context) =>  const GoogleMap.MyApp(),
        //   // '/List Types/Back Drop' : (context) =>  const ListType.HomePage(),
        //   // '/GetX Example' : (context) => const GetEx.HomePage(),
        // },
      ),
    );
  }
}
//
// class MyHomePage extends StatefulWidget {
//   const MyHomePage({Key? key}) : super(key: key);
//
//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }
//
// class _MyHomePageState extends State<MyHomePage> {
//   List<String> filteredData = [];
//
//   @override
//   void initState() {
//     super.initState();
//     filteredData.addAll(data);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text("Flutter App"),
//       ),
//       body: Column(
//         children: [
//           Padding(
//             padding: const EdgeInsets.all(16.0),
//             child: TextField(
//               onChanged: (value) {
//                 _serachItem(value);
//               },
//               decoration: InputDecoration(
//                 border: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(30),
//                 ),
//                 hintText: "Search Here...",
//               ),
//             ),
//           ),
//           Expanded(
//             child: ListView.builder(
//               itemCount: filteredData.length,
//               itemBuilder: (context,i) {
//                 return Card(
//                   elevation: 0,
//                   child: Container(
//                     decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(30),
//                         gradient: const LinearGradient(
//                           begin: Alignment.bottomLeft,
//                           end: Alignment.topRight,
//                           colors: [
//                             Colors.blue,
//                             Colors.deepPurple,
//                             // Colors.purple,
//                             Colors.red,
//                           ],
//                         )
//                     ),
//                     height: 70,
//                     child: InkWell(
//                       onTap: () {
//                         Navigator.pushNamed(context, "/${filteredData[i]}");
//                       },
//                       child: Center(child: Text(filteredData[i],style: const TextStyle(fontSize: 26, color: Colors.white),)),
//                     ),
//                   ),
//                 );
//               },
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   void _serachItem(String value) {
//
//     filteredData.clear();
//     for(String s in data) {
//       if(s.toLowerCase().contains(value.toLowerCase())) {
//         filteredData.add(s);
//       }
//     }
//     setState(() {});
//
//   }
// }